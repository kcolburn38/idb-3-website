FROM nikolaik/python-nodejs

COPY . /app

WORKDIR /app

RUN cd frontend && yarn install && yarn build
RUN cd backend && pip3 install -r requirements.txt

EXPOSE 80

CMD python3 backend/api.py 

