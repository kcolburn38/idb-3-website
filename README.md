# IDB 3 website
**Members**

**Kyle Colburn**
- kjc3328
- @kcolburn38

**Andrew Deng**
- aad2965
- @AndrewElvisDeng

**Franke Tang**
- ft4282
- @ftang21

**Alison Cheung**
- alc4953
- @alch021

**Trung Trinh**
- tnt2288
- @EatWorkSleep

**Alex Wu**
- aw345755
- @TheNecroreaper

**Git SHA**
- 86fca9695a0bebf3d8d2074cf100a21d1967dd90

**project leader**
Kyle Colburn

**link to GitLab pipelines**
[Pipelines](https://gitlab.com/kcolburn38/idb-3-website/-/pipelines)

**link to website**
[econyoom.me](https://www.econyoom.me/)

**Estimated Completion Time for Each Member: In hours**

- Kyle Colburn: 30
- Andrew Deng: 35
- Franke Tang: 25
- Alison Cheung: 30
- Trung Trinh: 40
- Alex Wu: 30

**Actual Completion Time for each Member: In hours**

- Kyle Colburn: 34
- Andrew Deng: 30 
- Franke Tang: 22
- Alison Cheung: 26
- Trung Trinh: 24
- Alex Wu: 25

**Comments**