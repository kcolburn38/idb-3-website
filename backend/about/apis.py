from flask import json, jsonify
import requests

# gets the apis used for the api section of about page
def get():
    apis = [
        {
            'img': "https://miro.medium.com/max/1002/1*tYLog4b5koGAWfQV859ff"+\
                "Q.png",
            'link': "https://developer.nrel.gov/docs/transportation/",
            'title': 'NREL',
            'info': "Used GET requests to grab data from API.",
            'cols': 2,
        },
        {
            'img': "https://lh3.googleusercontent.com/AW2Z_O3LTqlI973h7FvH3mW"+\
                "30sbV1Pc1BGovnnUKiFQ2j3pEQu4nKuX5Otqlk9xb_Kg",
            'link': "https://www.fueleconomy.gov/feg/ws/",
            'title': 'Fuel Economy',
            'info': "Used GET requests to grab data from API.",
            'cols': 2,
        },
        {
            'img': "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f"+\
                "/Energy_Information_Administration_logo.svg/1200px-Energy_In"+\
                "formation_Administration_logo.svg.png",
            'link': "https://www.eia.gov/opendata/qb.php?category=371",
            'title': 'EIA',
            'info': "Used GET requests to grab data from API.",
            'cols': 2,
        },
        {
            'img': "https://cdn.discordapp.com/attachments/53740332653949747"+\
                "2/773311215224619018/JOwnMgAAAAAElFTkSuQmCC.png",
            'link': "https://developers.google.com/maps/documentation/embed/"+\
                "get-started?hl=en_US",
            'title': 'Google Maps Embed API',
            'info': "Used to grab maps to embed on pages.",
            'cols': 2,
        },
        {
            'img': "https://cdn.discordapp.com/attachments/5374033265394974"+\
                "72/773314606847229962/gmgn0nnvnfuaagowmnj8.png",
            'link': "https://serpapi.com/",
            'title': 'SERP API',
            'info': "Used to grab images for vehicles and energy sources",
            'cols': 2
        },
        {
            'img': "https://cdn.discordapp.com/attachments/5374033265394974"+\
                "72/773314901116059688/gitlab-icon-rgb.png",
            'link': "https://docs.gitlab.com/ee/api/",
            'title': 'GitLab API',
            'info': "Used to grab info for the About page",
            'cols': 2
        }
    ]
    return json.jsonify(apis)