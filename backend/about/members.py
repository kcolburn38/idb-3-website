from flask import json, jsonify
import requests

# gets the data for the members section of about page


def get():
    # maps emails to members
    emails = {
        'kylecolburn38@utexas.edu': 'Kyle Colburn',
        'adeng1433@gmail.com': 'Andrew Deng',
        'alexwu2005@gmail.com': 'Alex Wu',
        'franketang@utexas.edu': 'Franke Tang',
        '55422893+FTang21@users.noreply.github.com': 'Franke Tang',
        'ttrinh1.bisd@gmail.com': 'Trung Trinh',
        'ls@LAPTOP-NF4V8DA1.localdomain': 'Trung Trinh',
        'calison@DESKTOP-9D79SE3.localdomain': 'Alison Cheung',
        'alisoncheung021@gmail.com': 'Alison Cheung',
        'aliceebrabbit@gmail.com': 'Alison Cheung',
    }

    # return dictionary with hard-set values
    members = {
        'Kyle Colburn': {
            'uname': 'kcolburn38',
            'img': '../images/Kyle.png',
            'commits': 0,
            'unit tests': 50,
            'bio': "Systems nerd extraordinaire, I also play for the UT Varsi" +
              "ty Overwatch team and love sleeping with my cat, Ink. He’s a" +
              " real homie. I like watching anime and Youtube in my limited" +
              " free time.",
            'role': 'De-Facto project lead, project documentation, and backen' +
              'd work.',
            'leader': True,
            'linkedin' : 'https://www.linkedin.com/in/kyle-colburn-167ba1163/',
        },
        'Andrew Deng': {
            'uname': 'AndrewElvisDeng',
            'img': '../images/Andrew.png',
            'commits': 0,
            'unit tests': 34,
            'bio': "3rd year UTCS alongside a Japanese certificate. MCU, Zel" +
              "da, FF, KH, Smash, Anime, Manga, music, culinary nerd. I’m a" +
              " lead player in The Pans of Texas, UT’s steel drum band. No" +
              "wadays, I spend my nonexistent free time hyping myself up " +
              "for upcoming game releases.",
            'role': 'Project meeting pacemaker, backend, devops',
            'leader': False,
            'linkedin' : 'https://www.linkedin.com/in/andrew-deng/',
        },
        'Alex Wu': {
            'uname': 'TheNecroreaper',
            'img': '../images/Alex.png',
            'commits': 0,
            'unit tests': 21,
            'bio': "Hello! I'm a third year CS student at UT Austin. Most of" +
                " my days are spent on my passion video games, developing or" +
                " playing them. If you ever catch me outside, I'll most like" +
                "ly be at the UT Archery range.",
            'role': 'Backend, DB',
            'leader': False,
            'linkedin' : 'https://www.linkedin.com/in/alwu/',
        },
        'Franke Tang': {
            'uname': 'ftang21',
            'img': '../images/Franke.png',
            'commits': 0,
            'unit tests': 27,
            'bio': "I’m a second year CS student at UT Austin. I love video " +
                "games and my dog Chewie, a Siberian Husky. Other interests " +
                "lies in Anime, DnD, Running, Ultimate Frisbee, Cooking, and" +
                " a slew of different other things.",
            'role': 'Frontend',
            'leader': False,
            'linkedin' : 'https://www.linkedin.com/in/franke-tang-684a341a3/',
        },
        'Trung Trinh': {
            'uname': 'EatWorkSleep',
            'img': '../images/Trung.png',
            'commits': 0,
            'unit tests': 9,
            'bio': "4th year Computer Science UT student. I enjoy video gami" +
                "ng and watching horror movies. Other interests include anim" +
                "e, cooking, bouldering, and Brazilian Jiu-jitsu (I’m not th" +
                "at good at the physical activities, but I enjoy them).",
            'role': 'Frontend',
            'leader': False,
            'linkedin' : 'https://www.linkedin.com/in/trung-trinh-277823195/',
        },
        'Alison Cheung': {
            'uname': 'alch021',
            'img': '../images/Alison.png',
            'commits': 0,
            'unit tests': 9,
            'bio': "I'm a fourth-year Computer Science student at UT. I take " +
              "way too many naps and watch too much anime. Currently, playi" +
              "ng too many rhythm games.",
            'role': 'Frontend',
            'leader': False,
            'linkedin' : 'https://www.linkedin.com/in/alison-cheung-065582180/',
        }
    }

    # get member issues
    for member in members:
        member_dict = members[member]
        uname = member_dict['uname']
        issues = requests.get(
            f'https://gitlab.com/api/v4/projects/21238733/issues_statistics?' +
            f'assignee_username={uname}').json()
        member_dict['issues'] = issues['statistics']['counts']['closed']

    # get member commits
    commits = requests.get(
        "https://gitlab.com/api/v4/projects/21238733/repository/commits?" +
        "per_page=1000").json()
    for commit in commits:
        members[emails[commit['author_email']]]['commits'] += 1

    return json.jsonify(members)
