from flask import json, jsonify
import requests

# gets the tools used for the tools section of about page


def get():
    tools = [
        {
            'img': "https://blog.qualys.com/wp-content/uploads/2020/10/post" +
            "man.jpg",
            'link': "https://www.postman.com/",
            'title': 'Postman',
            'info': "Used for API testing, design, and documentation",
            'cols': 2,
        },
        {
            'img': "https://user-images.githubusercontent.com/674621/711878" +
            "01-14e60a80-2280-11ea-94c9-e56576f76baf.png",
            'link': "https://code.visualstudio.com/",
            'title': 'VS Code',
            'info': "Used for the majority of coding by almost everyone. VS" +
            " Share was often used to collaborate on things in real time.",
            'cols': 2,
        },
        {
            'img': "https://www.kindpng.com/picc/m/163-1636552_color-icon-h" +
            "tml-css-js-bootstrap-hd-png.png",
            'link': "https://getbootstrap.com/",
            'title': 'Bootstrap',
            'info': "Used to create and stylize our web pages with HTML and " +
            "CSS.",
            'cols': 2,
        },
        {
            'img': "https://www.ilovedevops.com/img/avatar-icon.png",
            'link': "https://gitlab.com/",
            'title': 'GitLab',
            'info': "Used to host all the code on and version control.",
            'cols': 2,
        },
        {
            'img': "https://cdn.discordapp.com/attachments/537403326539497472" +
            "/773312151984930816/gtUmtvtEjBbAAAAABJRU5ErkJggg.png",
            'link': "https://www.namecheap.com/",
            'title': 'Namecheap',
            'info': "Used to get our web domain.",
            'cols': 2,
        },
        {
            'img': "https://miro.medium.com/max/4000/1*b_al7C5p26tbZG4sy-C" +
            "Wqw.png",
            'link': "https://aws.amazon.com/",
            'title': 'AWS',
            'info': "Used to host the web content on, specifically we are us" +
            "ing ‘Amplify’ to host our pages on to connect to our domain.",
            'cols': 2,
        },
        {
            'img': "https://miro.medium.com/max/800/1*Q5EUk28Xc3iCDoMSkrd1_w." +
            "png",
            'link': "https://flask.palletsprojects.com/en/1.1.x/",
            'title': 'Flask',
            'info': "The main backend server running utility, this is what we" +
            " use to host our app on.",
            'cols': 2,
        },
        {
            'img': "https://jestjs.io/img/jest.png",
            'link': "https://jestjs.io/",
            'title': 'Jest',
            'info': "Used to write Javascript unit tests.",
            'cols': 2,
        },
        {
            'img': "https://www.selenium.dev/images/selenium_logo_square_gree" +
            "n.png",
            'link': "https://www.selenium.dev/",
            'title': 'Selenium',
            'info': "Used to write GUI tests.",
            'cols': 2,
        },
        {
            'img': "https://blog.addthiscdn.com/wp-content/uploads/2014/11/ad" +
            "dthis-react-flux-javascript-scaling.png",
            'link': "https://reactjs.org/",
            'title': 'React',
            'info': "Used to create components to build our site.",
            'cols': 2,
        },
        {
            'img': "https://cdn.discordapp.com/attachments/5374033265394974" +
            "72/773312879159279666/oNWxjDsLPHkAAAAASUVORK5CYII.png",
            'link': "https://react-bootstrap.github.io/",
            'title': 'React Bootstrap',
            'info': "Used for navbar and containers.",
            'cols': 2,
        },
        {
            'img': "https://material-ui.com/static/logo.png",
            'link': "https://material-ui.com/",
            'title': 'Material UI',
            'info': "Used for pagination, tables, and grids.",
            'cols': 2,
        },
        {
            'img': "https://www.docker.com/sites/default/files/d8/styles/ro" +
            "le_icon/public/2019-07/vertical-logo-monochromatic.png?ito" +
            "k=erja9lKc",
            'link': "https://www.docker.com/",
            'title': 'Docker',
            'info': "Used to build/deploy our website.",
            'cols': 2,
        },
        {
            'img': "https://www.educative.io/api/edpresso/shot/57575820817858" +
            "56/image/5707702298738688",
            'link': "https://aws.amazon.com/ec2/",
            'title': 'AWS EC2',
            'info': "The aws service we are using to host the website on. Ru" +
            "ns both the front and back-end segments of the code.",
            'cols': 2,
        },
        {
            'img': "https://i.pinimg.com/originals/77/88/25/778825be381be66d" +
            "b27d56ba533034ea.png",
            'link': "https://www.python.org/",
            'title': 'Python',
            'info': "All of our backend is written in python, including the" +
            " API and database data scripts.",
            'cols': 2,
        },
        {
            'img': "https://img.stackshare.io/service/1839/q5uAkmy7.png",
            'link': "https://www.sqlalchemy.org/",
            'title': 'SQLAlchemy',
            'info': "A python library we use in the backend to connect to " +
            "our postgreSQL database and send/receive data from it.",
            'cols': 2,
        },
        {
            'img': "https://nickjanetakis.com/assets/blog/cards/flask-libra" +
            "ries-for-building-awesome-real-world-restful-apis-909c9f55" +
            "ee81f2a90344da2985159812423196d09a106d936534312ffa10be50.jpg",
            'link': "https://flask-restful.readthedocs.io/en/latest/",
            'title': 'Flask Restful',
            'info': "A python library we use in the backend to set up our AP" +
            "I on the flask app, it makes creating api calls easy but l" +
            "acks some deep flexibility.",
            'cols': 2,
        },
        {
            'img': "https://i.pinimg.com/originals/77/88/25/778825be381be66" +
            "db27d56ba533034ea.png",
            'link': "https://docs.python.org/3/library/unittest.html",
            'title': 'unittest',
            'info': "A tool we use to create unit tests for our backend fla" +
            "sk app.",
            'cols': 2,
        },
        {
            'img': "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9Gc" +
            "RcoqYEvRREKR8xi3y8_Ey9KNUoIZD6SeKFXA&usqp=CAU",
            'link': "https://cloud.google.com/sql/docs/postgres",
            'title': 'Postgres SQL',
            'info': "We have our database hosted on google cloud platform" +
            ", and it's in postgreSQL.",
            'cols': 2,
        },
        {
            'img': "https://res.cloudinary.com/hilnmyskv/image/upload/q_au" +
            "to/v1604064568/Algolia_com_Website_assets/images/shared/a" +
            "lgolia_logo/algolia-blue-mark.png",
            'link': "https://www.algolia.com/",
            'title': 'Algolia',
            'info': "Used to handle searching in a grid format and whole-si" +
            "te searching.",
            'cols': 2,
        },
        {
            'img': "https://pbs.twimg.com/profile_images/1136901164039991297/" +
            "-Vt-vAYQ.png",
            'link': "https://dbdiagram.io/home",
            'title': 'dbdiagram.io',
            'info': "Used to create a diagram of this site's database",
            'cols': 2,
        },
        {
            'img': "https://avatars3.githubusercontent.com/u/1562726?s=400&v=4",
            'link': "https://d3js.org/",
            'title': 'd3',
            'info': "Used to create visualizations",
            'cols': 2,
        },
    ]
    return json.jsonify(tools)
