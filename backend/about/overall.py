from flask import json, jsonify
import requests

# gets the data for the overall section of about page
def get():
    overall = {}
    # getting commits
    commits = requests.get(
        "https://gitlab.com/api/v4/projects/21238733/repository/commits?per_" +
        "page=1000").json()
    overall["Total Commits"] = len(commits)

    # getting issues
    overall["Total Issues"] = 0
    for i in range(2):
        issues = requests.get(
            f'https://gitlab.com/api/v4/projects/21238733'+
            f'/issues?per_page=100&page={i+1}').json()
        overall["Total Issues"] += len(issues)

    # getting unit tests
    overall["Total Unit Tests"] = 150
    return json.jsonify(overall)
