from unittest import main, TestCase
from ..models import energy_sources, infrastructure, uri, vehicles
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

# -----------
# TestTables
# -----------
testVehicle = vehicles.Vehicles(
    id=1,
    name="test",
    car_type="test",
    manufacturer="test",
    fuel_type="test",
    fuel_id=1,
    annual_cost=1,
    mpg=1.0,
    model_year=1,
    emissions=1.0,
    range_mi=1.0,
    epa_fuel_economy_score=1.0,
    engine_size="test",
    drive_type="test",
    avg_savings=1,
    energy_source=["test"],
    vehicle_image_url="test",

)
testInfrastructure = infrastructure.Infrastructure(
    id=1,
    station_name="test",
    access="test",
    fuel_type="test",
    facility_type="test",
    address="test",
    cards_accepted="test",
    ev_network="test",
    state="test",
    zip_code="test",
    country="test",
    phone_number="test",
    status="test",
    energy_source=["test"]
)
testEnergySource = energy_sources.EnergySource(
    id=1,
    name="test",
    supported_vehicles=[1],
    supported_charging_stations=[1],
    renewability=True,
    total_power=1,
    electric_utility=1,
    independent_producers=1,
    all_commercial=1,
    output_energy_type="test",
    laws_incentives="test",
    co2_emissions=1,
    description="test",
    image_url="test",
    information_url="test",
)
testEnergySourceInfratructure = vehicles.EnergySourceInfrastructure(
    fuel_type="test",
    fuel_stations=[1],
    fuel_stations_names=[""],
    fuel_stations_addresses=[""]
)
testInfrastructureVehicle = infrastructure.InfrastructureVehicles(
    fuel_type="test",
    serviceable_vehicles=[1],
    serviceable_vehicles_names=[""],
    serviceable_vehicles_pictures=[""]
)


class TestTables (TestCase):
    # test_session tests if getURIString() returns a valid session string

    def test_session(self):
        try:
            engine = create_engine(uri.getURI())
            Session = sessionmaker(bind=engine)
            s = Session()
            s.close()
            self.assertTrue(True)
        except:
            self.assertTrue(False)

    # test_<table>_1 tests the creation of a record with an invalid ID
    # test_<table>_2 tests the creation of a record with a valid ID
    # test_<table>_3 tests the creation of a record with valid inputs
    # test_<table>_dict tests the dictionairy form of a record

    def test_vehicle_1(self):
        try:
            vehicles.Vehicles(
                id="a",
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)

    def test_vehicle_2(self):
        vehicles.Vehicles(
            id=1,
        )
        self.assertTrue(True)

    def test_vehicle_3(self):
        self.assertTrue(isinstance(testVehicle, vehicles.Vehicles))

    def test_vehicle_dict(self):
        testDict = {
            'id': 1,
            'name': "test",
            'car_type': "test",
            'manufacturer': "test",
            'fuel_type': "test",
            'fuel_id': 1,
            'annual_cost': 1,
            'mpg': 1.0,
            'model_year': 1,
            'emissions': 1.0,
            'range_mi': 1.0,
            'epa_fuel_economy_score': 1.0,
            'engine_size': "test",
            'drive_type': "test",
            'avg_savings': 1,
            'energy_source': ["test"],
            'vehicle_image_url': "test",
        }
        self.assertEqual(testVehicle.get_dict(), testDict)

    def test_infrastructure_1(self):
        try:
            infrastructure.Infrastructure(
                id="a",
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)

    def test_infrastructure_2(self):
        infrastructure.Infrastructure(
            id=1,
        )
        self.assertTrue(True)

    def test_infrastructure_3(self):
        self.assertTrue(isinstance(testInfrastructure,
                                   infrastructure.Infrastructure))

    def test_infrastructure_dict(self):
        testDict = {
            'id': 1,
            'station_name': "test",
            'access': "test",
            'fuel_type': "test",
            'facility_type': "test",
            'address': "test",
            'cards_accepted': "test",
            'ev_network': "test",
            'state': "test",
            'zip_code': "test",
            'country': "test",
            'phone_number': "test",
            'status': "test",
            'energy_source': ["test"],
        }
        self.assertEqual(testInfrastructure.get_dict(), testDict)

    def test_energy_source_1(self):
        try:
            energy_sources.EnergySource(
                id="a",
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)

    def test_energy_source_2(self):
        energy_sources.EnergySource(
            id=1,
        )
        self.assertTrue(True)

    def test_energy_source_3(self):
        self.assertTrue(isinstance(testEnergySource,
                                   energy_sources.EnergySource))

    def test_energy_source_dict(self):
        testDict = {
            'id': 1,
            'name': "test",
            'supported_vehicles': [1],
            'supported_charging_stations': [1],
            'renewability': True,
            'total_power': 1,
            'electric_utility': 1,
            'independent_producers': 1,
            'all_commercial': 1,
            'output_energy_type': "test",
            'laws_incentives': "test",
            'co2_emissions': 1,
            'description': "test",
            'image_url': "test",
            'information_url': "test",
        }
        self.assertEqual(testEnergySource.get_dict(), testDict)

    def test_energy_source_infrastructure_1(self):
        try:
            vehicles.EnergySourceInfrastructure(
                fuel_type=1,
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)

    def test_energy_source_infrastructure_2(self):
        vehicles.EnergySourceInfrastructure(
            fuel_type="test",
        )
        self.assertTrue(True)

    def test_energy_source_infrastructure_3(self):
        self.assertTrue(isinstance(
            testEnergySourceInfratructure, vehicles.EnergySourceInfrastructure))

    def test_energy_source_infrastructure_dict(self):
        testDict = {
            'fuel_type': "test",
            'fuel_stations': [1],
            'fuel_stations_names': [""],
            'fuel_stations_addresses': [""]
        }
        self.assertEqual(testEnergySourceInfratructure.get_dict(), testDict)

    def test_infrastructure_vehicles_1(self):
        try:
            infrastructure.InfrastructureVehicles(
                fuel_type=1,
            )
            self.assertFalse(True)
        except:
            self.assertTrue(True)

    def test_infrastructure_vehicles_2(self):
        infrastructure.InfrastructureVehicles(
            fuel_type="test",
        )
        self.assertTrue(True)

    def test_infrastructure_vehicles_3(self):
        self.assertTrue(isinstance(testInfrastructureVehicle,
                                   infrastructure.InfrastructureVehicles))

    def test_infrastructure_vehicles_dict(self):
        testDict = {
            'fuel_type': "test",
            'serviceable_vehicles': [1],
            'serviceable_vehicles_names': [""],
            'serviceable_vehicles_pictures': [""]
        }
        self.assertEqual(testInfrastructureVehicle.get_dict(), testDict)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
