# third party imports
from flask import Flask, render_template, request, send_from_directory, json, \
jsonify
from flask_cors import CORS
from flask_restful import Resource, Api, abort, reqparse
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine
from sqlalchemy.orm import sessionmaker

# local imports
# from backend.energy_sources import
from models import vehicles, infrastructure, energy_sources, uri
from about import apis, members, overall, tools

# set up flask app
app = Flask(__name__, static_folder="../frontend/build/static",
template_folder="../frontend/build")
CORS(app)
api = Api(app)

# Root routing
# code courtesy of Caitlin Lien:
# https://gitlab.com/caitlinlien/cs373-sustainability.git
@app.route('/', defaults={'path': ""})
@app.route('/<path:path>')
def get_index(path):
    return render_template("index.html")


# gets the data for vehicle with vehicle_id
class vehicle_id(Resource):
	def get(self, vehicle_id):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(vehicles.Vehicles).get(vehicle_id)
		data_dict = data.get_dict()
		if (data_dict['fuel_type']):
			s_dict = s.query(vehicles.EnergySourceInfrastructure)			\
			.get(data_dict['fuel_type']).get_dict()
			s.close()
			
			data_dict['fuel_stations'] = s_dict['fuel_stations']
			data_dict['fuel_stations_names'] = s_dict['fuel_stations_names']
			data_dict['fuel_stations_addresses'] = 							\
			s_dict['fuel_stations_addresses']

		# handles instances with no available information
		else:
			data_dict['fuel_stations'] = []
			data_dict['fuel_stations_names'] = []
			data_dict['fuel_stations_addresses'] = []
		return json.jsonify(data_dict)
	
# gets the data for fuel station with fs_id 
class fuel_station_id(Resource):
	def get(self, fs_id):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(infrastructure.Infrastructure).get(fs_id)
		data_dict = data.get_dict()
		s_dict = s.query(infrastructure.InfrastructureVehicles)				\
		.get(data_dict['fuel_type']).get_dict()
		s.close()

		data_dict['serviceable_vehicles'] = s_dict['serviceable_vehicles']
		data_dict['serviceable_vehicles_names'] = 							\
		s_dict['serviceable_vehicles_names']
		data_dict['serviceable_vehicles_pictures'] = 						\
		s_dict['serviceable_vehicles_pictures']
		
		return json.jsonify(data_dict)

# gets the data for the energy source with es_id
class energy_source_id(Resource):
	def get(self, es_id):
		
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(energy_sources.EnergySource).get(es_id)
		s.close()

		return json.jsonify(data.get_dict())

# gets the data for the energy source with es_name
class energy_source_name(Resource):
	def get(self, es_name):	
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(energy_sources.EnergySource).filter(energy_sources. \
    EnergySource.name == es_name)
		s.close()
		
		for i in data:
			return json.jsonify(i.get_dict())


# all data for all vehicles
class all_vehicles(Resource):
	def get(self):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(vehicles.Vehicles).all()
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict())
		return json.jsonify(dict_data)

# first page of vehicles for pagination
class first_vehicles(Resource):
	def get(self):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(vehicles.Vehicles).limit(15)
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict())
		return json.jsonify(dict_data)


# all data for all fuel stations
class all_fuel_stations(Resource):
	def get(self):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(infrastructure.Infrastructure).all()
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict())
		return json.jsonify(dict_data)

# first page of fuel stations for pagination
class first_fuel_stations(Resource):
	def get(self):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(infrastructure.Infrastructure).limit(25)
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict())
		return json.jsonify(dict_data)

# all data for all energy sources
class all_sources(Resource):
	def get(self):
		engine = create_engine(uri.getURI())
		Session = sessionmaker(bind=engine)

		s = Session()
		data = s.query(energy_sources.EnergySource).all()
		s.close()
		dict_data = []
		for i in data:
			dict_data.append(i.get_dict_model_page())
		return json.jsonify(dict_data)

# member data for about page
class about_members(Resource):
	def get(self):
		return members.get()
		
# tool data for about page
class about_tools(Resource):
	def get(self):
		return tools.get()

# overall data for about page
class about_overall(Resource):
	def get(self):
		return overall.get()

# api data for about page
class about_apis(Resource):
	def get(self):
		return apis.get()

# model pages
api.add_resource(all_vehicles, '/api/vehicles')
api.add_resource(first_vehicles, '/api/vehicles_first')
api.add_resource(all_fuel_stations, '/api/fuel_stations')
api.add_resource(first_fuel_stations, '/api/fuel_stations_first')
api.add_resource(all_sources, '/api/energy_sources')

# instance pages
api.add_resource(vehicle_id, 
'/api/vehicles/id=<int:vehicle_id>', endpoint='vehicle_id')
api.add_resource(fuel_station_id, 
'/api/fuel_stations/id=<int:fs_id>', endpoint='fs_id')
api.add_resource(energy_source_id, 
'/api/energy_sources/id=<int:es_id>', endpoint='es_id')
api.add_resource(energy_source_name,
'/api/energy_sources/name=<string:es_name>', endpoint='es_name')

# about page 
api.add_resource(about_members, '/api/about/members')
api.add_resource(about_tools, '/api/about/tools')
api.add_resource(about_overall, '/api/about/overall')
api.add_resource(about_apis, '/api/about/apis')

# runs site
if __name__ == "__main__": 
	app.run(host ='0.0.0.0', port = 80, debug = True) 
