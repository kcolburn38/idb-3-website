from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Boolean, String, Date, Float, ARRAY
from sqlalchemy import create_engine
from .uri import *

Base = declarative_base()

# Energy Source model
class EnergySource(Base):
    __tablename__ = 'energy sources'
    id = Column(Integer, primary_key=True)
    # Name of Source
    name = Column(String)
    # Vehicles that it can support
    supported_vehicles = Column(ARRAY(Integer))             
    # Charging stations that use/sell this energy
    supported_charging_stations = Column(ARRAY(Integer))    
    # Renewability
    renewability = Column(Boolean)               
    # Total Power generated (Monthly - As of April 2020)
    total_power = Column(Integer)                           
    # Electric Utility (Annually - 2019)
    electric_utility = Column(Integer)                      
    # Independent producers (Annually - 2019)
    independent_producers = Column(Integer)                 
    # All Commercial (Annually - 2019)
    all_commercial = Column(Integer)                        
    # Output Energy Type
    output_energy_type = Column(String)                     
    # Laws + incentives
    laws_incentives = Column(String)                        
    # Emissions 
    co2_emissions = Column(Integer)                         
    # Description
    description = Column(String)                            
    # ES Image URL
    image_url = Column(String)                              
    # Info Page URL
    information_url = Column(String)                        

    # returns dictionary of the Energy Source table  
    def get_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'supported_vehicles': self.supported_vehicles,
            'supported_charging_stations': self.supported_charging_stations,
            'renewability': self.renewability,
            'total_power': self.total_power,
            'electric_utility': self.electric_utility,
            'independent_producers': self.independent_producers,
            'all_commercial': self.all_commercial,
            'output_energy_type': self.output_energy_type,
            'laws_incentives': self.laws_incentives,
            'co2_emissions': self.co2_emissions,
            'description': self.description,
            'image_url': self.image_url,
            'information_url': self.information_url,
        }
    
    # returns a concise energy source model for model pages
    def get_dict_model_page(self):
        return {
            'id': self.id,
            'name': self.name,
            'renewability': self.renewability,
            'total_power': self.total_power,
            'electric_utility': self.electric_utility,
            'independent_producers': self.independent_producers,
            'all_commercial': self.all_commercial,
            'output_energy_type': self.output_energy_type,
            'laws_incentives': self.laws_incentives,
            'co2_emissions': self.co2_emissions,
            'description': self.description,
            'image_url': self.image_url,
            'information_url': self.information_url,
        }

# creating tables
engine = create_engine(getURI())
Base.metadata.create_all(engine, checkfirst= True)