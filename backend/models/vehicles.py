from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Boolean, String, Date, Float, ARRAY
from sqlalchemy import create_engine
from .uri import *

Base = declarative_base()

# Vehicles model
class Vehicles(Base):
    __tablename__ = 'vehicles'
    id = Column(Integer, primary_key=True)
    # Vehicle Name
    name = Column(String)                   
    # Vehicle Type
    car_type = Column(String)               
    # Manufacturer
    manufacturer = Column(String)           
    # Fuel Type
    fuel_type = Column(String)              
    fuel_id = Column(Integer)               
    # Annual Fuel Cost
    annual_cost = Column(Integer)           
    # Combined MPG (or equivalent)
    mpg = Column(Float)                     
    # Model Year
    model_year = Column(Integer)            
    # CO2 Emissions
    emissions = Column(Float)               
    # Range
    range_mi = Column(Float)                
    # EPA Fuel Economy Score
    epa_fuel_economy_score = Column(Float)  
    # Engine Size
    engine_size = Column(String)            
    # Drive Type
    drive_type = Column(String)             
    #  How much you save compared to an average car (positive is savings)
    avg_savings = Column(Integer)
    # which energy sources support this vehicle
    energy_source = Column(ARRAY(String))
    # Vehicle Image URL
    vehicle_image_url = Column(String)      

    # returns dictionary of the vehicles table
    def get_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'car_type': self.car_type,
            'manufacturer': self.manufacturer,
            'fuel_type': self.fuel_type,
            'fuel_id': self.fuel_id,
            'annual_cost': self.annual_cost,
            'mpg': self.mpg,
            'model_year': self.model_year,
            'emissions': self.emissions,
            'range_mi': self.range_mi,
            'epa_fuel_economy_score': self.epa_fuel_economy_score,
            'engine_size': self.engine_size,
            'drive_type': self.drive_type,
            'avg_savings': self.avg_savings,
            'energy_source': self.energy_source,
            'vehicle_image_url': self.vehicle_image_url,
        }

# Maps vehicle energy sources to infrastructure
class EnergySourceInfrastructure(Base):
    __tablename__ = 'energy source infrastructure'
    fuel_type = Column(String, primary_key=True)
    fuel_stations = Column(ARRAY(Integer))
    fuel_stations_names = Column(ARRAY(String))
    fuel_stations_addresses = Column(ARRAY(String))

    # returns a dictionary of all infrastructure/energy source associations
    def get_dict(self):
        return {
            'fuel_type': self.fuel_type,
            'fuel_stations': self.fuel_stations,
            'fuel_stations_names': self.fuel_stations_names,
            'fuel_stations_addresses': self.fuel_stations_addresses
        }

# creating tables
engine = create_engine(getURI())
Base.metadata.create_all(engine, checkfirst= True)