from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Boolean, String, Date, Float, ARRAY
from sqlalchemy import create_engine
from .uri import *

Base = declarative_base()

# Infrastructure model
class Infrastructure(Base):
    __tablename__ = 'infrastructure'
    id = Column(Integer, primary_key=True)
    station_name = Column(String)           # Station Name
    access = Column(String)                 # Access Days/Time
    fuel_type = Column(String)              # Fuel Type
    facility_type = Column(String)          # Facility Type
    address = Column(String)                # Facility Address
    cards_accepted = Column(String)         # Cards Accepted
    ev_network = Column(String)             # EV Network
    state = Column(String)                  # State
    zip_code = Column(String)               # Zip Code
    country = Column(String)                # Country
    phone_number = Column(String)           # Station Phone
    status = Column(String)                 # Status (Currently Opened/Planned)
    energy_source = Column(ARRAY(String))   # Where the renewable comes from

    # returns dictionary of the Infrastructure table        
    def get_dict(self):
        return {
            'id': self.id,
            'station_name': self.station_name,
            'access': self.access,
            'fuel_type': self.fuel_type,
            'facility_type': self.facility_type,
            'address': self.address,
            'cards_accepted': self.cards_accepted,
            'ev_network': self.ev_network,
            'state': self.state,
            'zip_code': self.zip_code,
            'country': self.country,
            'phone_number': self.phone_number,
            'status': self.status,
            'energy_source': self.energy_source,
        }

# Maps infrastructure to vehicles via fuel type
class InfrastructureVehicles(Base):
    __tablename__ = 'infrastructure vehicles'
    fuel_type = Column(String, primary_key=True)
    serviceable_vehicles = Column(ARRAY(Integer))
    serviceable_vehicles_names = Column(ARRAY(String))
    serviceable_vehicles_pictures = Column(ARRAY(String))
    # returns a dictionary of all infrastructure/vehicle associations
    def get_dict(self):
        return {
            'fuel_type': self.fuel_type,
            'serviceable_vehicles': self.serviceable_vehicles,
            'serviceable_vehicles_names': self.serviceable_vehicles_names,
            'serviceable_vehicles_pictures': self.serviceable_vehicles_pictures
        }        

# creating tables
engine = create_engine(getURI())
Base.metadata.create_all(engine, checkfirst= True)