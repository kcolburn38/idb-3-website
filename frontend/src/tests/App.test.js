import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { shallow } from 'enzyme';


// Import our pages/components
import App from '../App';
import Header from '../components/Header';
import NavBar from '../components/NavBar';
import About from '../pages/About';
import Vehicles from '../pages/Vehicles';
import Stations from '../pages/Stations';
import Energy from '../pages/Energy';
import SearchPage from '../pages/SearchPage';
import VehicleInstance from '../pages/VehicleInstance';
import StationInstance from '../pages/StationInstance';
import EnergyInstance from '../pages/EnergyInstance';
import MemberInfo from '../components/aboutComponents/MemberInfo';
import ApiToolInfo from '../components/aboutComponents/ApiToolInfo';
import ApiInfo from '../components/aboutComponents/ApiInfo';
import ToolInfo from '../components/aboutComponents/ToolInfo';
import EcoCarousel from '../components/homeComponents/EcoCarousel';
import Footer from '../components/Footer';
import Home from '../components/homeComponents/Home';

Enzyme.configure({ adapter: new Adapter() });

// Tests pages of our site

it('App renders w/o crashing and has info', () => {
  const page = shallow(<App />);
  const header = <Header />;
  const navbar = <NavBar />;
  expect(page.contains(header)).toEqual(true);
  expect(page.contains(navbar)).toEqual(true);
});


it('About renders w/o crashing and has info', () => {
  const page = shallow(<About />);
  const header = <h1 className="text-center">Motivation</h1>;
  const memberInfo = <MemberInfo />;
  const apiToolInfo = <ApiToolInfo />;
  expect(page.contains(memberInfo)).toEqual(true);
  expect(page.contains(apiToolInfo)).toEqual(true);
  expect(page.contains(header)).toEqual(true);
});


it('Stations page renders w/o crashing and has info', () => {
  const page = shallow(<Stations />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('Vehicles page renders w/o crashing and has info', () => {
  const page = shallow(<Vehicles />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('Energy page renders w/o crashing and has info', () => {
  const page = shallow(<Energy />);
  expect(page.find("InstantSearch")).toEqual({});
  expect(page.find("Pagination")).toEqual({});
});


it('VehicleInstance renders w/o crashing and has info', () => {
  const page = shallow(<VehicleInstance />);
  expect(page.find("MaterialTable")).toEqual({});
  expect(page.find("Container")).toEqual({});
  expect(page.find("div")).toEqual({});
});


it('StationInstance renders w/o crashing and has info', () => {
  const page = shallow(<StationInstance />);
  expect(page.find("MaterialTable")).toEqual({});
});


it('EnergyInstance renders w/o crashing and has info', () => {
  const page = shallow(<EnergyInstance />);
  expect(page.find("MaterialTable")).toEqual({});
});

it('Search page renders w/o crashing and has info', () => {
  const page = shallow(<SearchPage />);
  expect(page.find("InstantSearch")).toEqual({});
  expect(page.find("Pagination")).toEqual({});
  expect(page.find("VehicleContent")).toEqual({});
  expect(page.find("StationContent")).toEqual({});
  expect(page.find("EnergyContent")).toEqual({});
});

it('ApiToolInfo renders w/o crashing and has info', () => {
  const component = shallow(<ApiToolInfo />);
  expect(component.find("ApiInfo")).toEqual({});
  expect(component.find("ToolInfo")).toEqual({});
  expect(component.find("Grid")).toEqual({});
  expect(component.find("Button")).toEqual({});
});

it('ApiInfo renders w/o crashing and has info', () => {
  const component = shallow(<ApiInfo />);
  expect(component.find("GridList")).toEqual({});
  expect(component.find("Popover")).toEqual({});
  expect(component.find("Typography")).toEqual({});
});

it('ToolInfo renders w/o crashing and has info', () => {
  const component = shallow(<ToolInfo />);
  expect(component.find("GridList")).toEqual({});
  expect(component.find("Popover")).toEqual({});
  expect(component.find("Typography")).toEqual({});
});

it('EcoCarousel renders w/o crashing and has info', () => {
  const component = shallow(<EcoCarousel />);
  expect(component.find("Carousel")).toEqual({});
  expect(component.find("Carousel.Item")).toEqual({});
  expect(component.find("Carousel.Caption")).toEqual({});
});

it('Footer renders w/o crashing and has info', () => {
  const component = shallow(<Footer />);
  expect(component.find("RCFooter")).toEqual({});
});

it('Header renders w/o crashing and has info', () => {
    const component = shallow(<Header />);
    expect(component.find("div")).toEqual({});
    expect(component.find("h1")).toEqual({});
    expect(component.find("img")).toEqual({});
    expect(component.find("p")).toEqual({});
});

it('Home renders w/o crashing and has info', () => {
  const component = shallow(<Home />);
  expect(component.find("Grid")).toEqual({});
  expect(component.find("Link")).toEqual({});
  expect(component.find("Button")).toEqual({});
  expect(component.find("Card")).toEqual({});
});

it('MemberInfo renders w/o crashing and has info', () => {
  const component = shallow(<MemberInfo />);
  expect(component.find("Card")).toEqual({});
  expect(component.find("CardContent")).toEqual({});
  expect(component.find("Container")).toEqual({});
  expect(component.find("Typography")).toEqual({});
});

it('NavBar renders w/o crashing and has info', () => {
  const component = shallow(<NavBar />);
  expect(component.find("Navbar")).toEqual({});
  expect(component.find("Navbar.Brand")).toEqual({});
  expect(component.find("Nav.Link")).toEqual({});
});
