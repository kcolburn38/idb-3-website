from selenium import webdriver
from unittest import TestCase, main
import time
PATH = "../../../../chromedriver_win32/chromedriver.exe"
URL = "http://econyoom.me"
browser = webdriver.Chrome(PATH)

class guiTest(TestCase):

    def test_home(self):
        # make sure we load the home page correctly
        browser.get(URL)
        assert browser.current_url == URL + '/'
        assert browser.title == 'EcoNyoom'

        # make sure search button works
        browser.get(URL)
        search = browser.find_element_by_class_name('MuiButton-label')
        search.click()
        assert browser.current_url == URL + '/search'

        # make sure vehicles card works
        browser.get(URL)
        buttons = browser.find_elements_by_tag_name("button")
        buttons[2].click()
        assert browser.current_url == URL + '/vehicles'

        # make sure fuel stations card works
        browser.get(URL)
        buttons = browser.find_elements_by_tag_name("button")
        buttons[3].click()
        assert browser.current_url == URL + '/stations'

        # make sure energy sources card works
        browser.get(URL)
        buttons = browser.find_elements_by_tag_name("button")
        buttons[4].click()
        assert browser.current_url == URL + '/energy'

        # make sure footer link works
        browser.get(URL)
        footer = browser.find_element_by_link_text("Econyoom.me")
        footer.click()
        assert browser.current_url == URL + '/'


    def test_vehicle_model(self):
        browser.get(URL + '/vehicles')
        assert browser.current_url == URL + '/vehicles'

        # test search in table
        search = browser.find_element_by_tag_name('input')
        search.send_keys('teSLa mOdeL')

        # test filter in table
        filter = browser.find_element_by_class_name('MuiSelect-root')
        filter.click()

    def test_vehicle_instance(self):
        browser.get(URL + '/vehicles/12454')
        assert browser.current_url == URL + '/vehicles/12454'

        # test twitter link
        time.sleep(3)
        twitter = browser.find_element_by_class_name("twitter-timeline")
        twitter.click()
        assert (browser.current_url == 
            'https://twitter.com/Tesla?ref_src=twsrc%5Etfw')

        # test energy instance links
        browser.get(URL + '/vehicles/12454')
        browser.implicitly_wait(10)
        link = browser.find_element_by_link_text('Wind')
        link.click()
        assert browser.current_url == URL + '/energy/10'


    def test_infrastructure_model(self):
        browser.get(URL + '/stations')
        assert browser.current_url == URL + '/stations'

        # test search in table
        search = browser.find_element_by_tag_name('input')
        search.send_keys('tx h-e-b')

        # test filter in table
        filter = browser.find_element_by_class_name('MuiSelect-root')
        filter.click()


    def test_infrastructure_instance(self):
        browser.get(URL + '/stations/14104')
        assert browser.current_url == URL + '/stations/14104'

    def test_energy_model(self):
        browser.get(URL + '/energy')
        assert browser.current_url == URL + '/energy'
        
        # test energy model search
        search = browser.find_element_by_tag_name('input')
        search.send_keys('energy')

        # test links of grids
        time.sleep(2)
        links = browser.find_elements_by_tag_name('a')
        links[10].click()
        assert browser.current_url == URL + '/energy/10'

    def test_energy_intsance(self):
        browser.get(URL + '/energy/10')
        assert browser.current_url == URL + '/energy/10'


    def test_about_page(self):
        browser.get(URL + '/about')
        assert browser.current_url == URL + '/about'

        # test link to gitlab
        buttons = browser.find_elements_by_class_name("MuiButton-contained")
        buttons[0].click()
        assert (browser.current_url == 
            'https://gitlab.com/kcolburn38/idb-3-website')

        # test link to postman
        browser.get(URL + '/about')
        buttons = browser.find_elements_by_class_name("MuiButton-contained")
        buttons[1].click()
        assert (browser.current_url == 
            'https://documenter.getpostman.com/view/12831690/TVKHUaYQ')

    def test_search_page(self):
        browser.get(URL + '/search')
        assert browser.current_url == URL + '/search'

        # test search functionality
        search = browser.find_element_by_class_name('ais-SearchBox-input')
        search.send_keys('oil')
        
        # test switching model functionality
        time.sleep(1)
        choose = browser.find_element_by_class_name('MuiSelect-selectMenu')
        choose.click()
        choices = browser.find_elements_by_class_name('MuiListItem-button')
        choices[1].click()

        # test reset search functionality
        reset = browser.find_element_by_class_name('ais-SearchBox-reset')
        reset.click()
        search.send_keys('tesla')

if __name__ == "__main__":
    main()
    browser.close()
