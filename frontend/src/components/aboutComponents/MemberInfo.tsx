import React from 'react';
import useAxios from 'axios-hooks';
import Error from '../Error';
import loading from '../../images/loading.gif'

// react-bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// member images
import kyleImg from '../../images/Kyle.png';
import andrewImg from '../../images/Andrew.png';
import alexImg from '../../images/Alex.png';
import frankeImg from '../../images/Franke.png';
import trungImg from '../../images/Trung.png';
import alisonImg from '../../images/Alison.png';
import linkedin from '../../images/linkedin.png';

// Card tools from material ui
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';

// style for the Card
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    height: "100%",
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

// Display member info for about page
function MemberInfo() {

  const classes = useStyles();

  // Call to get member info
  const [{ data: memberInfo, loading: memberLoad, error: memberError }] =
    useAxios(`/api/about/members`);
  // Call to get total issues, commits, tests
  const [{ data: overall, loading: overallLoad, error: overallError }] =
    useAxios(`/api/about/overall`);

  // Handle loading/error messages
  if (memberLoad || overallLoad) return <div style={{ textAlign: 'center' }}>
    <img src={loading} alt='loading' className='center' /> </div>
  if (memberError || overallError) {
    return <Error />
  }

  // Returns number of total issues
  const getTotalIssues = () => {
    if (overall !== undefined) {
      return (
        overall['Total Issues']
      )
    }
  }

  // Returns number of total commits
  const getTotalCommits = () => {
    if (overall !== undefined) {
      return (
        overall['Total Commits']
      )
    }
  }

  // Returns number of total tests
  const getTotalTests = () => {
    if (overall !== undefined) {
      return (
        overall['Total Unit Tests']
      )
    }
  }

  // Map images to members
  let images = new Map();
  images.set('Kyle Colburn', kyleImg);
  images.set('Andrew Deng', andrewImg);
  images.set('Alex Wu', alexImg);
  images.set('Franke Tang', frankeImg);
  images.set('Trung Trinh', trungImg);
  images.set('Alison Cheung', alisonImg);

  // Checks if leader is true to label team lead
  const leader = (leader: boolean) => {
    if (leader) {
      return (
        <p style={{ textAlign: 'center' }}>★ Team Lead ★</p>
      )
    } else {
      return (
        <br />
      )
    }
  }

  // Make a member card passing member's name as a string
  const infoCard = (name: string) => {
    if (memberInfo !== undefined) {
      let arr = [];
      arr.push(
        <Card className={classes.root} key="name">
          <CardHeader
            title={name} titleTypographyProps={{ variant: 'h5' }}
            style={{ textAlign: 'center' }}
          />
          {/* display member data */}
          <CardContent>
            <img src={images.get(name)}
              className="my_img mx-auto d-block" alt="member img" />
            {leader(memberInfo[name]['leader'])}
            <Typography color="textSecondary" component="p">
              {memberInfo[name]['bio']}<br /><br />
              {memberInfo[name]['role']}
              <br />
              <br />
                Number of Issues: {memberInfo[name]['issues']}<br />
                Number of Commits: {memberInfo[name]['commits']}<br />
                Number of Tests: {memberInfo[name]['unit tests']}<br />
              <br />
              <a href={memberInfo[name]['linkedin']}>
                <img src={linkedin} alt="linkedin logo" style={{
                  width: "20px",
                  height: "20px", position: "absolute", bottom: "10px"
                }} />
              </a>
            </Typography>
          </CardContent>
        </Card>
      );
      return arr;
    }
  }

  return (
    <div>
      <Container>
        {/* displays overall project data */}
        <br />
        <h2 className="text-center">Overall Data</h2>
        <br />
        <Row>
          <Col>
            <p>Total Commits: {getTotalCommits()}</p>
          </Col>
          <Col>
            <p>Total Issues: {getTotalIssues()}</p>
          </Col>
          <Col>
            <p>Total Unit Tests: {getTotalTests()}</p>
          </Col>
        </Row>
        <br />
        <br />
        <h2 className="text-center">Member Data</h2>
        <br />
        {/* Creates member cards and displays as grid */}
        <Row>
          <Col>
            {infoCard('Kyle Colburn')}
          </Col>
          <Col>
            {infoCard('Andrew Deng')}
          </Col>
          <Col>
            {infoCard('Alex Wu')}
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            {infoCard('Franke Tang')}
          </Col>
          <Col>
            {infoCard('Trung Trinh')}
          </Col>
          <Col>
            {infoCard('Alison Cheung')}
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default MemberInfo
