import React from 'react'
import useAxios from 'axios-hooks';
import LoadImg from '../../images/loading.gif';
import Error from '../Error';

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

// Popover from Material ui
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';

// style for the Gridlist
const useStyles = makeStyles((theme) => ({
  gridList: {
    width: 500,
    height: 450,
  },
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: theme.spacing(1),
    border: '1px',
    borderStyle: 'solid',
    boxShadow: 'none',
  },
}));

// displays tool info in Gridlist
function ToolInfo() {
  const classes = useStyles();

  const [{ data, loading, error }] = useAxios('/api/about/tools');

  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const [info, setInfo] = React.useState<String | null>(null);

  // Handles mouse over events on api title for popover for use description
  const handlePopoverOpen = (event: React.MouseEvent<HTMLElement,
    MouseEvent>, data: String) => {
    setAnchorEl(event.currentTarget);
    setInfo(data);
  };

  // Handles closing popover
  const handlePopoverClose = () => {
    setAnchorEl(null);
    setInfo(null);
  };

  // Popover to display info on api use
  const popover = (str: String, data: String) => {
    let array = [];
    array.push(
      <Typography
        aria-owns={open ? 'mouse-over-popover' : undefined}
        aria-haspopup="true"
        onMouseEnter={(e) => handlePopoverOpen(e, data)}
        onMouseLeave={handlePopoverClose}
      >
        {str}
      </Typography>
    )
    return array;
  }

  const open = Boolean(anchorEl);

  // Handle loading/error message
  if (loading) return <div style={{ textAlign: 'center' }}>
    <img src={LoadImg} alt='loading' className='center' /> </div>
  if (error) {
    return <Error />
  }

  return (
    <div>
      {/* displays the Gridlist with tools we used */}
      <GridList cellHeight={180} className={classes.gridList}>
        <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
        </GridListTile>
        {data.map((tile: any) => (
          <GridListTile key={tile.img}>
            <a href={tile.link}>
              <img src={tile.img} alt={tile.title}
                style={{ width: "100%", height: "100%" }} />
            </a>
            <GridListTileBar
              key={tile.title + tile.link}
              title={popover(tile.title, tile.info)}
            />
            {/* places popover around our info bar */}
            <Popover
              id="mouse-over-popover"
              className={classes.popover}
              classes={{
                paper: classes.paper,
              }}
              open={open}
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              onClose={handlePopoverClose}
              disableRestoreFocus
            >
              <Typography>{info}</Typography>
            </Popover>
          </GridListTile>
        ))}
      </GridList>
    </div>
  )
}

export default ToolInfo;
