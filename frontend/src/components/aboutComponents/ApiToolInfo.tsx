import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Grid from '@material-ui/core/Grid'

import Button from '@material-ui/core/Button';

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';

import ApiInfo from './ApiInfo';
import ToolInfo from './ToolInfo';

import GitLabLogo from '../../images/gitlab.png';
import PostManLogo from '../../images/postman.png';

// style for the Gridlists
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
}));

// Display both api and tool info
function ApiToolInfo() {
  const classes = useStyles();

  return (
    <div>
      <br />
      <hr />
      <br />
      <Container>
        {/* display our two gridlists side-by-side */}
        <div className={classes.root}>
          <Col>
            <br />
            <h2 className="text-center">APIs</h2>
            <ApiInfo />
          </Col>
          <Col>
            <br />
            <h2 className="text-center">Tools</h2>
            <ToolInfo />
          </Col>
        </div>
      </Container>
      <Container>
        <br />
        <hr />
        <br />
        <Row>
          {/* display our gitlab repo and postman api as buttons */}
          <Col>
            <Grid container justify="center" >
              <a href="https://gitlab.com/kcolburn38/idb-3-website">
                <img src={GitLabLogo} alt="gitlab logo"
                  style={{ width: "40px", height: "40px" }} />
              </a>
              <Button variant="contained" color="primary"
                style={{ color: '#FFF' }}
                href="https://gitlab.com/kcolburn38/idb-3-website">
                GitLab Repo
              </Button>
            </Grid>
            <br />
            <Grid container justify="center" >
              <a href=
                "https://documenter.getpostman.com/view/12831690/TVKHUaYQ">
                <img src={PostManLogo} alt="postman logo"
                  style={{ width: "35px", height: "35px" }} />
              </a>
              <Button variant="contained" color="primary"
                style={{ marginLeft: "5px", color: '#FFF' }}
                href=
                "https://documenter.getpostman.com/view/12831690/TVKHUaYQ">
                Postman API
              </Button>
            </Grid>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default ApiToolInfo
