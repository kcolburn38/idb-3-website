import React from 'react';
import RCFooter from 'rc-footer';
import 'rc-footer/assets/index.css'; // import 'rc-footer/assets/index.less';
import carLogo from '../images/favicon-32x32.png';

// Displays the footer for our website
function Footer() {
  return (
    <div>
      <RCFooter
        columns={[
          {
            title: 'About Us',
            items: [
              {
                title:
                  <p>
                  The purpose this site is to display information
                  about ecological vehicles, <br />infrastructure in places
                  within the U.S. which supports ecological vehicles, as 
                  <br />
                  well as the many sources of energy that we use to power
                  not only our <br /> vehicles but also our everyday lives.
                </p>,
              },
            ],
          },
          {
            title: 'Models',
            items: [
              { title: 'Vehicles', url: '/vehicles', },
              { title: 'Fueling Stations', url: '/stations', },
              { title: 'Energy Sources', url: '/energy', },
            ],
          },
          {
            title: 'Our Data',
            items: [
              {
                title: 'GitLab Repo',
                url: 'https://gitlab.com/kcolburn38/idb-3-website',
              },
              {
                title: 'Postman API',
                url: 'https://documenter.getpostman.com/view/12831690/TVKHUaYQ',
              },
            ],
          },
          {
            title: 'More',
            items: [
              { title: 'Home', url: '/', },
              { title: 'About', url: '/about', },
              { title: 'Search', url: '/search', }
            ],
          },
        ]}
        bottom={<div><p>Copyright © 2020 EcoNyoom <img src={carLogo}
          alt='favicon' style={{ width: 20, height: 20 }} /></p></div>}
        backgroundColor="#2e8b57"
      />
    </div>
  );
}

export default Footer
