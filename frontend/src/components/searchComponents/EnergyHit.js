import React from "react";
import PropTypes from "prop-types";
import { Highlight } from "react-instantsearch-dom";
import { Link } from "react-router-dom";


export default EnergyHit;

// List of our fields in the energy model
const fields = ["renewability", "total_power", "electric_utility",
  "independent_producers", "output_energy_type", "co2_emissions"];

// Maps fields to a labeling
const fieldNames = {
  "renewability": "Renewability",
  "total_power": "Total Power (in k MWh)",
  "electric_utility": "Electric Utility (in k MWh)",
  "independent_producers": "Independent Producers (in k MWh)",
  "output_energy_type": "Output Energy Type", "co2_emissions": "CO2 Emissions"
};

// Contains a searchable energy instance
function EnergyHit({ hit }) {
  // Uses the energy fields to display/return our data
  const returnData = () => {
    return fields.map(fields => (<div className={"hit-" + fields}>
      <p> <b>{fieldNames[fields]}: </b>
        <Highlight attribute={fields}
          hit={hit} />
      </p>
    </div>
    ));
  }

  return (
    <div className="hit">
      {/* link the whole card to the instance page */}
      <Link to={'/energy/' + hit.id} className="link_style">
        <div className="hit-content">
          <div className="hit-image">
            <img src={hit.image_url} alt="hit img"
              width="200px" height="100px" />
          </div>
          <div>
            {/* displays searchable data (all attributes) 
              complete with highlights */}
            <div className="hit-name">
              <b><Highlight attribute="name" hit={hit} /></b>
            </div>
            <div className="hit-description">
              <p><Highlight attribute="description" hit={hit} /></p>
            </div>
            {returnData()}
          </div>
        </div>
      </Link>
    </div>
  );
}

EnergyHit.propTypes = {
  hit: PropTypes.object.isRequired
};
