import React from "react";
import PropTypes from "prop-types";
import { Highlight } from "react-instantsearch-dom";
import { Link } from "react-router-dom";


export default StationHit;

// List of our fields in the station model
const fields = ["access", "fuel_type", "facility_type", "address", "state",
  "zip_code", "energy_source"];

// Maps fields to a labeling
const fieldNames = {
  "access": "Access Days/Time", "fuel_type": "Fuel Type",
  "facility_type": "Facility Type", "address": "Facility Address",
  "state": "State", "zip_code": "Zip Code", "energy_source":
    "Origin of Energy Source"
};

// Contains a searchable station instance
function StationHit({ hit }) {

  // Uses the station fields to display/return our data
  const returnData = () => {
    return fields.map(fields => (<div className={"hit-" + fields}>
      <p><b>{fieldNames[fields]}: </b>
        <Highlight attribute={fields}
          hit={hit} />
      </p>
    </div>
    ));
  }
  return (
    <div className="hit">
      {/* link the whole card to the instance page */}
      <Link to={'/stations/' + hit.id} className="link_style">
        <div className="hit-content">
          <div>
            {/* displays searchable data (all attributes) 
              complete with highlights */}
            <div className="hit-station_name">
              <b><Highlight attribute="station_name" hit={hit} /></b>
            </div>
            {returnData()}
          </div>
        </div>
      </Link>
    </div>
  );
}

StationHit.propTypes = {
  hit: PropTypes.object.isRequired
};
