import React from "react";
import PropTypes from "prop-types";
import { Highlight } from "react-instantsearch-dom";
import { Link } from "react-router-dom";


export default VehicleHit;

// List of our fields in the vehicle model
const fields = ["car_type", "manufacturer", "fuel_type", "mpg", "model_year",
  "drive_type", "avg_savings", "energy_source"];

// Maps fields to a labeling
const fieldNames = {
  "car_type": "Car Type", "manufacturer": "Manufacturer",
  "fuel_type": "Fuel Type", "mpg": "Combined MPG (or equivalent)",
  "model_year": "Model Year", "drive_type": "Drive Type", "avg_savings":
    "Average Savings (in USD)", "energy_source": "Energy Sources"
};

// Contains a searchable vehicle instance
function VehicleHit({ hit }) {

  // Uses the vehicles fields to display/return our data
  const returnData = () => {
    return fields.map(fields => (<div className={"hit-" + fields}>
      <p> <b>{fieldNames[fields]}: </b>
        <Highlight attribute={fields}
          hit={hit} />
      </p>
    </div>
    ));
  }

  return (
    <div className="hit">
      {/* link the whole card to the instance page */}
      <Link to={'/vehicles/' + hit.id} className="link_style">
        <div className="hit-content">
          <div>
            <img src={hit.vehicle_image_url} alt="hit img"
              width="300px" height="200px" />
          </div>
          <div>
            {/* displays searchable data (all attributes) 
              complete with highlights */}
            <div className="hit-name">
              <b><Highlight attribute="name" hit={hit} /></b>
            </div>
            {returnData()}
          </div>
        </div>
      </Link>
    </div>
  );
}

VehicleHit.propTypes = {
  hit: PropTypes.object.isRequired
};
