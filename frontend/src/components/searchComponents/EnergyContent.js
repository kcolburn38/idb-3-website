import React from "react";
import { Hits } from "react-instantsearch-dom";
import { connectStateResults } from "react-instantsearch/connectors";
import EnergyHit from "./EnergyHit.js";


// Puts hits into searchable results
export default connectStateResults(
  ({ searchState, searchResults }) =>
    searchResults && searchResults.nbHits !== 0 ? (
      <Hits hitComponent={EnergyHit} />
    ) : (
        <div>
          No results found for <strong>{searchState.query}</strong>.
        </div>
      )
);
