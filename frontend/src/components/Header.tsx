import React from 'react';
import { Link } from "react-router-dom";
import logo from '../images/EcoNyoom.png';

// Display our header for the website
function Header() {
  return (
    <div>
      <Link to="/">
        <img src={logo} alt="volcano" style={{ width: 250, height: 100 }} />
      </Link>
      <p className="lead">
        Ecological Vehicles For Your Ecological Needs
      </p>
    </div>
  )
}

export default Header;
