import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Typed from 'react-typed';
import carImg from '../images/vehicle_1.png';
import carImg2 from '../images/vehicle_2.png';
import carImg3 from '../images/vehicle_3.png';
import stationImg from '../images/stations_1.png';
import stationImg2 from '../images/stations_2.png';
import stationImg3 from '../images/stations_3.png';
import arrow from '../images/down_arrow.gif';


// Displays the vehicle image above our table
export default function VehicleImage({ myRef, isCar }) {

  // Event handler to scroll to table bottom
  const scrollToBottom = () => {
    if (typeof (myRef) !== 'undefined') {
      if (myRef.current) {
        myRef.current.scrollIntoView({
          behavior: "smooth", block: "nearest"
        })
      }
    }
  }

  // Shows either a car or station depending on the bool
  const showImg = (isCar) => {
    let imgSrc1, imgSrc2, imgSrc3;
    let altString1, altString2, altString3;
    if (isCar) {
      imgSrc1 = carImg;
      altString1 = "car1";
      imgSrc2 = carImg2;
      altString2 = "car2";
      imgSrc3 = carImg3;
      altString3 = "car3";
    } else {
      imgSrc1 = stationImg;
      altString1 = "station1";
      imgSrc2 = stationImg2
      altString2 = "station2";
      imgSrc3 = stationImg3;
      altString3 = "station3";
    }
    return (
      // Displays the three images
      <Container fluid={true}>
        <Row>
          <img src={imgSrc1} alt={altString1} className="my_model mx-auto 
            d-block" style={{ marginTop: '60px' }} />
          <img src={imgSrc2} alt={altString2} className="my_model mx-auto 
            d-block" style={{ marginTop: '60px' }} />
          <img src={imgSrc3} alt={altString3} className="my_model mx-auto 
            d-block" style={{ marginTop: '60px' }} />
        </Row>
        {/* Displays the arrow */}
        <img src={arrow} alt="arrow" style={{
          display: "block",
          margin: "0 auto", width: "5%", height: "5%"
        }} onClick={() => scrollToBottom()} />

      </Container>
    )
  }

  // Show typing of text information based on bool
  const showTyped = (isCar) => {
    // Set strings array to display
    let stringArr;
    if (isCar) {
      stringArr = ['Explore our cars!', 'Just head below!',
        'And search through our table!'];
    } else {
      stringArr = [
        'Explore our stations!',
        'Just head below!',
        'And search through our table!'];
    }
    return (
      // Returns typing with given array
      <Typed
        strings={stringArr}
        typeSpeed={40}
        backSpeed={50}
        loop
        style={{
          fontSize: '30px', display: 'flex',
          justifyContent: 'center', paddingTop: '40px'
        }}
      >
      </Typed>
    )
  }

  return (
    // Displays the typing and images
    <div style={{ backgroundColor: '#a2ded0', margin: "0 auto" }}>
      {showTyped(isCar)}
      {showImg(isCar)}
    </div>
  );
}
