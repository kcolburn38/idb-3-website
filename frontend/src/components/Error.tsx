import React from 'react';

// Returns error message
function Error() {
  return (
    <div>
      <p style={{ textAlign: "center", marginBottom: 150, marginTop: 150 }}>
        There was an issue trying to retrieve your data. Try again.
            </p>
    </div>
  )
}

export default Error
