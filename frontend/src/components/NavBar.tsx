import React from 'react';
import { Navbar, Nav } from 'react-bootstrap/';
import lightning from '../images/lightning_bolt.svg';

// Displays navbar for website
function NavBar() {
  return (
    <div>
      <Navbar bg="light" variant="light" expand="lg" 
          style={{ fontSize: 18 }}>
        {/* displays image on navbar */}
        <Navbar.Brand href="/">
          <img
            src={lightning}
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        {/* displays navbar links for our pages in the site */}
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/vehicles">Vehicles</Nav.Link>
            <Nav.Link href="/stations">Fueling Stations</Nav.Link>
            <Nav.Link href="/energy">Energy</Nav.Link>
            <Nav.Link href="/visualizations">Visualizations</Nav.Link>
          </Nav>
          <Nav className="inline">
            <Nav.Link href="/search">Search</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  )
}

export default NavBar
