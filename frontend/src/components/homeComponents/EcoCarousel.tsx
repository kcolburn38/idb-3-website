import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import item3 from '../../images/carousel_1.png';
import item2 from '../../images/carousel_2.jpg';
import item1 from '../../images/carousel_3.jpg';

// Displays the carousel on home page
function EcoCarousel() {
  return (
    <div style={{ backgroundColor: '#a2ded0', margin: "0 auto" }}>
      <div className="container">
        <Carousel>
          {/* displays first slide with image and text */}
          <Carousel.Item style={{ height: "550px" }}>
            <img
              className="d-block w-100"
              src={item1}
              alt="First slide"
            />
            <Carousel.Caption style={{ background: "rgba(0, 0, 0, 0.4)" }}>
              <h3>Come join us to...</h3>
              <p>Find an ecological car!</p>
            </Carousel.Caption>
          </Carousel.Item>
          {/* displays second slide with image and text */}
          <Carousel.Item style={{ height: "550px" }}>
            <img
              className="d-block w-100"
              src={item2}
              alt="Second slide"
            />
            <Carousel.Caption style={{ background: "rgba(0, 0, 0, 0.4)" }}>
              <h3>To...</h3>
              <p>Learn about energy sources cars use!</p>
            </Carousel.Caption>
          </Carousel.Item>
          {/* displays third slide with image and text */}
          <Carousel.Item style={{ height: "550px" }}>
            <Carousel.Caption style={{ background: "rgba(0, 0, 0, 0.4)" }}>
              <h3>And to...</h3>
              <p>Explore your options for a greener future!</p>
            </Carousel.Caption>
            <img
              className="d-block w-100"
              src={item3}
              alt="Third slide"
            />
          </Carousel.Item>
        </Carousel>
      </div>
    </div>
  )
}

export default EcoCarousel
