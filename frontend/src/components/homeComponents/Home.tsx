import React from 'react';
import '../../css/style.css';
import { Link } from "react-router-dom";
import HomeCards from "./HomeCards";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'

// Display the body of our home page
function Home() {

  return (
    <div>
      <hr />
      <br />
      {/* display information explaining what site is about */}
      <h2 className="font-weight-light text-center">Come explore!</h2>
      <br />
      <Grid container justify="center" >
        <Link to="/search">
          <Button variant="contained" color="primary">
            Search Our Site!
          </Button>
        </Link>
      </Grid>
      <br />
      <p className="text-center">Here you can look through our
      vehicles and see their price as well as their
      orignating energy sources. You can also find fueling
      stations in your area and learn about all the
      different kinds of energies out there!
      </p>
      <br />
      {/* display our three models as cards */}
      <div>
        <HomeCards />
      </div>
    </div>
  )
}

export default Home
