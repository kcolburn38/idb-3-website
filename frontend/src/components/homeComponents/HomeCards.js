import React from 'react';
import '../../css/style.css';
import { Link } from "react-router-dom";

//images 
import homeCar from '../../images/home_car_2.jpg';
import station from '../../images/charging_1.jpg';
import energy from '../../images/home_energy_3.jpg';

// react-bootstrap to lay things out better
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Card tools from material ui
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import { red } from '@material-ui/core/colors';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';

// style for the Card
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

// Our three models
const fields = ['vehicle', 'station', 'energy'];

// Necessary info of our models to make the cards
const attributes = {
  'vehicle': ['Vehicles', homeCar, 'vehicles', 'Explore vehicles!'],
  'station': ['Fuel Stations', station, 'stations', 'Explore stations!'],
  'energy': ['Energy Sources', energy, 'energy', 'Explore energy!'],
};

// Display the cards on our home page
function HomeCards() {
  const classes = useStyles();

  // Creates and returns our cards with the list and dict
  const returnCards = () => {
    return fields.map(fields => (
      <Col>
        <Card className={classes.root}>
          <CardHeader
            title={attributes[fields][0]} titleTypographyProps=
            {{ variant: 'h4' }}
          />
          <Link to={'/' + attributes[fields][2]} >
            <CardMedia
              className={classes.media}
              image={attributes[fields][1]}
              title={fields}
            />
          </Link>
          <CardActions>
            <Link to={'/' + attributes[fields][2]} >
              <Button size="small" color="primary">
                {attributes[fields][3]}
              </Button>
            </Link>
          </CardActions>
        </Card>
      </Col>
    ));
  }

  return (
    <Container>
      {/* display our three models as cards */}
      <Row>
        {returnCards()}
      </Row>
      <br />
      <br />
    </Container>
  )
}

export default HomeCards
