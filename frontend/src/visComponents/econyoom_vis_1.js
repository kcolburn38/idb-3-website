// Imports
import React, { useCallback } from "react";
import * as d3 from "d3";

function Vis1(props) {
    const data = [];
    d3.csv('https://raw.githubusercontent.com/comannnnndooooo/idb3' +
      '-raw-data/main/vs1.csv', function(d) {
      data.push({label: d['energy_source'], value: parseInt(d['count'])});
    }).then(d => {
      drawChart();
    });
    const outerRadius = props.o;
    const innerRadius = props.i;

    const margin = {
      top: 50, right: 50, bottom: 50, left: 50,
    };
    const width = 2 * outerRadius + margin.left + margin.right;
    const height = 2 * outerRadius + margin.top + margin.bottom;

    const colorScale = d3     
    .scaleSequential()      
    .interpolator(d3.interpolateWarm)      
    .domain([0, data.length+1]);
  
    const drawChart = useCallback(() => {
      // Remove the old svg
        d3.select('#pie-container')
        .select('svg')
        .remove();
        
      // Create new svg
      const svg = d3
          .select('#pie-container')
          .append('svg')
          .attr('width', width)
          .attr('height', height)
          .append('g')
          .attr('transform', `translate(${width / 2}, ${height / 2})`);

      const arcGenerator = d3
          .arc()
          .innerRadius(innerRadius)
          .outerRadius(outerRadius);

      const pieGenerator = d3
          .pie()
          .padAngle(0)
          .value((d) => d.value);

      const arc = svg
          .selectAll()
          .data(pieGenerator(data))
          .enter();

      // Append arcs
      arc
          .append('path')
          .attr('d', arcGenerator)
          .style('fill', (_, i) => colorScale(i/6))
          .style('stroke', '#333333')
          .style('stroke-width', 1);

      // Append text labels
      arc
          .append('text')
          .attr('text-anchor', 'middle')
          .attr('alignment-baseline', 'middle')
          .text((d) => d.data.label)
          .style('fill', '#333333')
          .style('font-weight', "bold")
          .style("font-size", "18px")
          .attr('transform', (d) => {
          const [x, y] = arcGenerator.centroid(d);
          return `translate(${x} , ${y} )`;
          });
      
      const legend = svg
          .append('g')
          .attr('transform', `translate(${width + 20},0)`);

      var labelHeight = 18;

      legend
          .selectAll(null)
          .data(pieGenerator(data))
          .enter()
          .append('rect')
          .attr('y', d => labelHeight * d.index * 1.8)
          .attr('width', labelHeight)
          .attr('height', labelHeight)
          .attr('fill', d => colorScale(d.index))
          .attr('stroke', 'grey')
          .style('stroke-width', '1px');
      
      legend
          .selectAll(null)
          .data(pieGenerator(data))
          .enter()
          .append('text')
          .text(d => d.data.key)
          .attr('x', labelHeight * 1.2)
          .attr('y', d => labelHeight * d.index * 1.8 + labelHeight)
          .style('font-family', 'sans-serif')
          .style('font-size', `${labelHeight}px`);
    }, [colorScale, data, height, innerRadius, outerRadius, width]);
  
    return <div id="pie-container" />;
  }

  export default Vis1;
