// Imports
import React, { useCallback } from "react";
import * as d3 from "d3";

function Vis3(props) {
    const data = [];

    d3.csv('https://raw.githubusercontent.com/comannnnndooooo/idb3-' +
      'raw-data/main/vs3.csv', function(d) {
      data.push({label: d['fuel_type'], value: parseInt(d['mpg'])});
    }).then(d => {
      drawChart();
    });

      
    // Creating Constants to display the data
    var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 40
      },
      width = 1080 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

    const x = d3.scalePoint()
      .domain(["Biodiesel (B20)", "Hydrogen Cell", 
        "Compressed Natural Gas", "CNG - Bi-fuel",
        "Electric", "Ethanol (E85)", "Propane", "PHEV", 
        "Propane - Bi-fuel", "Hybrid Electric", "Methanol"])
      .range([40, width]);
      
    const y = d3.scaleLinear()
      .range([height, margin.top]);
      
    const center = d3.scaleLinear()
      .range([0, width]);

    const xAxis = d3.axisBottom(x);
    const yAxis = d3.axisLeft(y).ticks(20);

    const centerLine = d3.axisTop(center).ticks(0);
  
    // Displaying the data
    const drawChart = useCallback(() => {
      // Remove the old svg
      d3.select('#vs3-container')
      .select('svg')
      .remove();

      // Compute the min and max savings values to display
      const y_min = d3.min(data, function(data) { return data['value'] });
      const y_max = d3.max(data, function(data) { return data['value'] + 5 });
      
      // use them to determine the y axis domain
      y.domain([y_min, y_max]);

      // Start making the chart to display by starting with the background
      var svg = d3.select("#vs3-container").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left 
          + "," + margin.top + ")");
        
      // Add in the bottom x axis
      svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

      // Add in the left y axis
      svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

      // Add in the center line
      svg.append("g")
			  .attr("class", "centerline")
			  .attr("transform", "translate(0," + y(0) + ")")
        .call(centerLine);

      // Get all the rectangles placed with the right data 
      svg.selectAll(".entry")
        .data(data)
        .enter().append("rect")
        .attr("class", "g")
        .attr("transform", function(d) {
          return "translate(" + (data.indexOf(d) * 97 + 25) + ", 0)"; })
			  .attr("width", 50)
			  .attr("y", function(d) {
          if(d.value >= 0){ 
            d.top = d.value;
            d.bottom = 0;
            return y(d.value); 
          } 
          d.top = 0
          d.bottom = d.value
          return y(0);
        })
			  .attr("height", function(d) { 
          return Math.abs(y(d.top) - y(d.bottom));            
        })
			  .style("fill", function (d) {
          if(d.value < 0) { return "#8B0000"; }
          return "#006400";
        } );

    }, [centerLine, data, height, margin, width, y, xAxis, yAxis]);
    return <div id="vs3-container" />;
  }

  export default Vis3;
