// Imports
import React, { useCallback } from "react";
import * as d3 from "d3";

function Vis4(props) {
    const data = [];
    d3.csv('https://raw.githubusercontent.com/comannnnndooooo/idb3-raw-da' +
      'ta/main/vs4.csv', function(d) {
      data.push({label: d['num_officers'], value: parseInt(d['pop'])});
    }).then(d => {
      drawChart();
    });
    

      
    // Creating Constants to display the data
    var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 60
      },
      width = 1080 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;
  
    // Displaying the data
    const drawChart = useCallback(() => {
        // Remove the old svg
        d3.select('#vs4-container')
        .select('svg')
        .remove();

        var svg = d3.select("#vs4-container")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");
                
         // Add X axis
        var x = d3.scaleLinear()
            .domain([0, 5000])
            .range([ 0, width ]);           
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        // Add Y axis
        var y = d3.scaleLinear()
            .domain([0, 1000000])
            .range([ height, 0]);
        svg.append("g")
            .call(d3.axisLeft(y));    
            
        // Add dots
        svg.append('g')
            .selectAll("dot")
            .data(data)
            .enter()
            .append("circle")
            .attr("cx", function (d) { return x(d.label); } )
            .attr("cy", function (d) { return y(d.value); } )
            .attr("r", 1.5)
            .style("fill", "#1A04FC")

    }, [data, height, margin, width]);
    return <div id="vs4-container" />;
  }

  export default Vis4;
