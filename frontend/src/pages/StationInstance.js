import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MaterialTable from "material-table";
import {
  Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import LoadImg from '../images/loading.gif';
import tempImg from '../images/boltEV.jpg';
import Error from '../components/Error';

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

// style for the Gridlist
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: '#a2ded0',
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into own layer on Chrome.
    transform: 'translateZ(0)',
  },
  title: {
    color: theme.palette.common.white,
    "&:hover": {
      color: "#5468ff"
    },
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, ' +
      'rgba(0,0,0,0) 100%)',
  },
}));

// List of our fields in the station
const fields1 = ["access", "cards_accepted", "ev_network", "facility_type",
  "fuel_type", "phone_number"];

// List of our fields in the station
const fields2 = ["status", "address", "state", "zip_code", "country"];

// Maps fields to a labeling
const fieldNames = {
  "access": "Access", "cards_accepted": "Cards Accepted",
  "ev_network": "EV Network", "facility_type": "Facility Type",
  "fuel_type": "Fuel Type", "phone_number": "Phone #", "status": "Status",
  "address": "Address", "state": "State", "zip_code": "Zip Code",
  "country": "Country"
};

// To display our station instance
function StationInstance() {

  const classes = useStyles();

  // Grabs page url and then station id
  let pages = window.location.pathname.split('/');
  let id = pages[2];

  const [{ data, loading, error }] = useAxios(`/api/fuel_stations/id=${id}`);
  const [tileData, setTileData] = useState([]);

  useEffect(() => {
    if (data !== undefined) {
      let array = data['energy_source'];
      // Puts the energy source instances into a grid list to display
      if (array !== undefined) {
        for (let i = 0; i < array.length; i++) {
          let info = [];
          axios.get(`/api/energy_sources/name=${array[i]}`)
            .then((r) => {
              info.push(
                {
                  img: r.data['image_url'],
                  title: array[i],
                  info: "",
                  cols: 2,
                  id: r.data["id"],
                },
              )
              setTileData((prevState) => prevState.concat(info));
            });
        }
      }
    }
  }, [data]);

  if (loading) return <div style={{ textAlign: 'center' }}>
    <img src={LoadImg} alt='loading' className='center' /> </div>
  if (error) {
    return <Error />
  }

  const vehicleRows = () => {
    // Creates the rows for our vehicle table
    if (data['serviceable_vehicles'] !== undefined) {
      let rows = [];
      for (let v = 0; v < data['serviceable_vehicles'].length; v++) {
        rows.push({
          // adds the vehicle id, name, and image
          vehicle_id: data['serviceable_vehicles'][v],
          vehicle_name: data['serviceable_vehicles_names'][v],
          vehicle_image: data['serviceable_vehicles_pictures'][v]
        });
      }
      return rows;
    }
  }

  // Column for vehicle table
  const vehicleColumns = [
    {
      title: 'Vehicles',
      field: 'vehicle_name',
      render: rowData =>
        <div style={{ textAlign: "center" }}>
          <Link to={'/vehicles/' + rowData['vehicle_id']}>
            <p>{rowData['vehicle_name']}</p>
            <img src={rowData['vehicle_image']} alt="car"
              style={{ width: "400px", height: "200px" }}
              onError=
              {(e) => { e.target.onerror = null; e.target.src = tempImg }} />
          </Link>
        </div>
    },
  ];

  // Icons for our material table
  const tableIcons = {
    Search: forwardRef((props, ref) =>
      <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) =>
      <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) =>
      <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) =>
      <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) =>
      <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) =>
      <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) =>
      <ArrowDownward {...props} ref={ref} />),
  };

  // Checks default value of null to display N/A
  const checkNull = (value) => {
    if (value === null) {
      return "N/A";
    }
    return value;
  }

  // Formats the an address to get our map location
  const formatAddress = (data) => {
    let address = data['address'];
    if (address !== undefined) {
      address = address.replaceAll(" ", "+");
      address = address.concat(",");
      address = address.concat(data['state']);
      address = address.concat("+");
      address = address.concat(data['zip_code']);
      address = address.concat('+');
      address = address.concat(data['country']);
      return address;
    }
  }

  // Uses the station fields to display/return our data
  const returnData = (fields) => {
    return fields.map(fields => (
      <p>
        <strong>{fieldNames[fields]}: </strong>
        {checkNull(data[fields])}
      </p>
    ));
  }

  // Return iframe of map
  const returnMap = () => {
    return (
      <iframe
        title="map"
        width="600"
        height="450"
        frameBorder="0" style={{
          border: "0", marginTop: '20px',
          marginBottom: '40px'
        }}
        src={"https://www.google.com/maps/embed/v1/place?key=" +
          "AIzaSyDdWiM3yF5ZagF745-dELROOc0Vs7LiK2k&q=" +
          formatAddress(data)}
        allowFullScreen>
      </iframe>
    )
  }

  return (
    <div>
      <Container fluid={true} style={{ backgroundColor: '#a2ded0' }}>
        <br />
        <h2 className="text-center">{data['station_name']}</h2>
        <Row>
          <Col style={{ textAlign: 'center' }}>
            {/* display for our map embed */}
            {returnMap()}
          </Col>
        </Row>
        <Row style={{ backgroundColor: '#ffffff' }}>
          {/* displays our station instance data */}
          <Col>
            <br />
            {returnData(fields1)}
          </Col>
          <Col>
            <br />
            {returnData(fields2)}
          </Col>
        </Row>
        <br />
        <h4>Powered by: </h4>
        {/* displays our energy sources as a gridlist */}
        <div className={classes.root}>
          <GridList className={classes.gridList} cols={2.5}>
            {tileData.map((tile) => (
              <GridListTile key={tile.img} style={{ width: 300 }}>
                <Link to={"/energy/" + tile.id}>
                  <img src={tile.img} alt={tile.title}
                    style={{ width: '100%', height: '100%' }} />
                  <GridListTileBar
                    title={tile.title}
                    classes={{
                      root: classes.titleBar,
                      title: classes.title,
                    }}
                  />
                </Link>
              </GridListTile>
            ))}
          </GridList>
        </div>
        {/* displays material table of our vehicles */}
        <br />
        <MaterialTable title="Servicable Vehicles" data={vehicleRows()}
          columns={vehicleColumns} icons={tableIcons}
          options={{
            exportButton: false,
            headerStyle: { backgroundColor: '#01579b', color: '#FFF' },
          }} />
        <br />
      </Container>
    </div>
  )
}

export default StationInstance;
