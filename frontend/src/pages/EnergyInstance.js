import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import wikiLogo from '../images/wiki-logo.png'
import eiaLogo from '../images/eia.png'
import MaterialTable from "material-table";
import {
  Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import LoadImg from '../images/loading.gif';
import tempImg from '../images/boltEV.jpg';
import Error from '../components/Error';

// List of our attributes in energy instance
const fields1 = ["total_power", "electric_utility", "independent_producers"];
const fields2 = ["all_commercial", "output_energy_type", "co2_emissions"];

// Maps fields to a labeling
const fieldNames = {
  "total_power": "Total Power (in k MWh)", "electric_utility":
    "Electric Utility (in k MWh)", "independent_producers":
    "Independent Producers (in k MWh)", "all_commercial":
    "All Commercial (in k MWh)", "output_energy_type": "Output Energy Type",
  "co2_emissions": "CO2 Emissions"
};

// To display our energy instance
function EnergyInstance() {
  let pages = window.location.pathname.split('/');
  let id = pages[2];

  // Uses the station fields to display/return our data
  const returnData = (fields) => {
    return fields.map(fields => (
      <p>
        <strong>{fieldNames[fields]}: </strong> {checkNull(data[fields])}
      </p>
    ));
  }

  const [{ data, loading, error }] = useAxios(`/api/energy_sources/id=${id}`);
  const [vehicleRows, setVehicleRows] = useState([]);
  const [stationRows, setStationRows] = useState([]);

  useEffect(() => {
    if (data !== undefined) {
      // Get some supported vehicles for our table
      if (data['supported_vehicles'] !== undefined) {
        for (let v = 0; v < 5 && data['supported_vehicles'].length; v++) {
          let vehicle_id = data['supported_vehicles'][v];
          let rows = [];
          axios.get(`/api/vehicles/id=${vehicle_id}`)
            .then((r) => {
              // adds vehicle id, name, and image
              rows.push({
                vehicle_id: r.data['id'],
                vehicle_name: r.data['name'],
                vehicle_image: r.data['vehicle_image_url']
              });
              setVehicleRows((prevState) => prevState.concat(rows));
            });
        }
      }

      // Get some supported charging stations for our table
      if (data['supported_charging_stations'] !== undefined) {
        for (let s = 0; s < 5 && data
        ['supported_charging_stations'].length; s++) {
          let station_id = data['supported_charging_stations'][s];
          let rows = [];
          axios.get(`/api/fuel_stations/id=${station_id}`)
            .then((r) => {
              // add station id, name, and address
              rows.push({
                station_id: station_id,
                station_name: r.data['station_name'],
                station_map: r.data['address']
              });
              setStationRows((prevState) => prevState.concat(rows));
            });
        }
      }
    }
  }, [data]);

  if (loading) return <div style={{ textAlign: 'center' }}>
    <img src={LoadImg} alt='loading' className='center' /> </div>
  if (error) {
    return <Error />
  }

  // Used to print out our boolean
  const checkTrue = (bool) => {
    if (bool) {
      return 'True';
    }
    return 'False';
  }

  // Checks null to display N/A
  const checkNull = (value) => {
    if (value === null) {
      return "N/A";
    }
    return value;
  }

  // Handles exception for wood using external info from wiki
  const getLink = (name) => {
    if (name === "Wood and Wood Derived Fuels") {
      return wikiLogo;
    }
    return eiaLogo;
  }

  // Returns the image of energy
  const returnImage = () => {
    return (
      <div style={{ textAlign: 'center' }}>
        <img src={data['image_url']} alt={data['name'] + "image"}
          className="my_model mx-auto d-block" git
          style={{
            marginTop: '20px', marginBottom: '60px',
            height: 300, width: 300
          }} />
      </div>
    )
  }

  // Makes our columns for vehicle and station table
  const vehicleColumns = [
    {
      title: 'Vehicles',
      field: 'vehicle_name',
      render: rowData =>
        <div style={{ textAlign: "center" }}>
          <Link to={'/vehicles/' + rowData['vehicle_id']}>
            <p>{rowData['vehicle_name']}</p>
            <img src={rowData['vehicle_image']} alt="car"
              style={{ width: "400px", height: "200px" }}
              onError={(e) => { e.target.onerror = null; e.target.src = tempImg }} />
          </Link>
        </div>
    },
  ];

  const stationColumns = [
    {
      title: 'Name',
      field: 'station_name',
      render: rowData => <Link to={'/stations/' + rowData['station_id']}>
        {rowData.station_name}</Link>,
    },
    {
      title: 'Map',
      field: 'station_map',
      render: rowData =>
        <iframe
          title="map"
          width="300"
          height="225"
          frameBorder="0" style={{ border: "0" }}
          src={"https://www.google.com/maps/embed/v1/place?key=" +
            "AIzaSyDdWiM3yF5ZagF745-dELROOc0Vs7LiK2k&q=" +
            rowData.station_map}
          allowFullScreen>
        </iframe>
    },
  ];

  // Icons for our matieral ui tables
  const tableIcons = {
    Search: forwardRef((props, ref) =>
      <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) =>
      <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) =>
      <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) =>
      <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) =>
      <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) =>
      <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) =>
      <ArrowDownward {...props} ref={ref} />),
  };

  return (
    <div>
      <Container fluid={true} style={{ backgroundColor: '#a2ded0' }}>
        <br />
        <h2 className="text-center">{data['name']}</h2>
        <Row>
          <Col style={{ height: 400 }}>
            {returnImage()}
          </Col>
        </Row>
        {/* display description and more info */}
        <Row style={{ backgroundColor: '#ffffff' }}>
          <Col>
            <br />
            <p className="text-center h4">Description</p>
            <hr />
            <p className="text-center">{data["description"]}</p>
            <br />
          </Col>
        </Row>
        {/* displays our attribute data */}
        <Row style={{ backgroundColor: '#ffffff' }}>
          <Col>
            <p><strong>Renewability:</strong> {checkTrue(
              data["renewablility"])}</p>
            {returnData(fields1)}
          </Col>
          <Col>
            {returnData(fields2)}
            <p><strong>More Info: </strong> <a href={data['information_url']}>
              <img src={getLink(data['name'])} width="20" height="20"
                alt="info-img" /> </a></p>
          </Col>
        </Row>
        <br />
        {/* displays our material tables */}
        <MaterialTable title="Five Random Supported Vehicles"
          data={vehicleRows} columns={vehicleColumns}
          icons={tableIcons} options={{ exportButton: false }}
          localization={{
            body: {
              emptyDataSourceMessage:
                <img src={LoadImg} alt="loading" />
            }
          }} />
        <br />
        <MaterialTable title="Five Random Supported Stations"
          data={stationRows} columns={stationColumns}
          icons={tableIcons} options={{ exportButton: false }}
          localization={{
            body: {
              emptyDataSourceMessage:
                <img src={LoadImg} alt="loading" />
            }
          }} />
        <br />
      </Container>
    </div>
  )
}

export default EnergyInstance;
