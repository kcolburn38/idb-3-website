import React from 'react';
import algoliasearch from 'algoliasearch/lite';
import Grid from '@material-ui/core/Grid'
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import "../css/search.css";
import { makeStyles } from '@material-ui/core/styles';

// import for algolia search
import {
  InstantSearch,
  SearchBox,
  Pagination,
  Configure
} from "react-instantsearch-dom";
import EnergyContent from "../components/searchComponents/EnergyContent";
import VehicleContent from "../components/searchComponents/VehicleContent";
import StationContent from "../components/searchComponents/StationContent";

// style for the Dropdown
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

// Displays our model searches
function SearchPage() {

  const classes = useStyles();
  const [model, setModel] = React.useState('ec_energy_source_NAME');

  const handleChange = (event) => {
    setModel(event.target.value);
  };

  // pick which model to search
  const chooseContent = () => {
    if (model === "ec_vehicles_NAME") {
      return <VehicleContent />
    } else if (model === "ec_infrastructure_NAME") {
      return <StationContent />
    } else {
      return <EnergyContent />
    }
  }

  // keys for our algolia search
  const searchClient = algoliasearch(
    'S78GCYPFBZ',
    '4480af22f8ec00f9f09c5db37b4fe245'
  );

  return (
    <div>
      {/* displays dropdown box */}
      <Grid container justify="center">
        <FormControl className={classes.formControl}>
          <Select
            value={model}
            onChange={handleChange}
            displayEmpty
            className={classes.selectEmpty}
            inputProps={{ 'aria-label': 'Without label' }}
          >
            <MenuItem value="" disabled>
              Models
            </MenuItem>
            <MenuItem value="ec_vehicles_NAME">Vehicles</MenuItem>
            <MenuItem value="ec_infrastructure_NAME">Stations</MenuItem>
            <MenuItem value="ec_energy_source_NAME">Energy Sources</MenuItem>
          </Select>
        </FormControl>
      </Grid>

      {/* displays algolia search  */}
      <InstantSearch indexName={model}
        searchClient={searchClient}>
        <main className="search-container">
          <Configure
            hitsPerPage={6}
            attributesToSnippet={["description:24"]}
            snippetEllipsisText=" [...]"
          />
          {/* displays the search box */}
          <div className="search-panel">
            <div id="searchbox">
              {<SearchBox
                translations={{ placeholder: "Search here" }}
              />}
            </div>
            {/* displays energy content */}
            <div id="hits">
              {chooseContent()}
            </div>
            {/* displays pagination */}
            <div id="pagination">
              {<Pagination />}
            </div>
          </div>
        </main>
      </InstantSearch>
    </div>
  )
}

export default SearchPage;

