import React from 'react';
import algoliasearch from 'algoliasearch/lite';
import "../css/search.css";


// import for algolia search
import {
  InstantSearch,
  SearchBox,
  Pagination,
  Configure,
  Panel,
  SortBy
} from "react-instantsearch-dom";
import Content from "../components/searchComponents/EnergyContent";
import Facet from "../components/searchComponents/Facet";

// Display our energy model page
function Energy() {

  // keys for our algolia search
  const searchClient = algoliasearch(
    'S78GCYPFBZ',
    '4480af22f8ec00f9f09c5db37b4fe245'
  );

  return (

    // displays algolia search 
    <InstantSearch indexName="ec_energy_source_NAME" 
      searchClient={searchClient}>
      <main className="search-container">
        <Configure
          hitsPerPage={6}
          attributesToSnippet={["description:24"]}
          snippetEllipsisText=" [...]"
        />
        {/* displays the search box */}
        <div className="search-panel">
          <div id="searchbox">
            {<SearchBox
              translations={{ placeholder: "Search energy sources" }}
            />}
          </div>
          <div id="sortby">
              {/* Uncomment the following widget to add categories list */}
              { <SortBy
                  defaultRefinement="ec_energy_source_NAME"
                  items={[
                    { value: 'ec_energy_source_despower_NAME', 
                      label: 'Highest power' },
                    { value: 'ec_energy_source_ascpower_NAME', 
                      label: 'Lowest power' },
                    { value: 'ec_energy_source_co2asc_NAME', 
                      label: 'Highest C02 Emissions' },
                    { value: 'ec_energy_source_co2des_NAME', 
                      label: 'Lowest C02 Emissions' }
                  ]}
                /> }
            </div>
          {/* displays energy content */}
          <div id="hits">
            {<Content />}
          </div>
          {/* displays pagination */}
          <div id="pagination">
            {<Pagination />}
          </div>
        </div>
        <div className="left-panel">
            <div id="Renewability">
              {/* Uncomment the following widget to add categories list */}
              { <Panel header="renewability">
                <Facet attribute="renewability" />
              </Panel> }
            </div>
            <div id="outputenergytype">
              {/* Uncomment the following widget to add categories list */}
              { <Panel header="Output Energy Type">
                <Facet attribute="output_energy_type" />
              </Panel> }
            </div>
            </div>
      </main>
    </InstantSearch>
  )
}

export default Energy;

