import React, { forwardRef, useEffect, useState } from 'react';
import useAxios from 'axios-hooks';
import MaterialTable from "material-table";
import {
  Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear,
  ArrowDownward, FilterList
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import loading from '../images/loading.gif';
import Highlighter from "react-highlight-words";
import MTableFilterRow from '../components/filterOveride'
import ModelImage from '../components/ModelImage';
import Error from '../components/Error';

// Displays our vehicle model page
function Vehicles() {

  const [myRef] = useState(React.createRef());
  const [tableRef] = useState(React.createRef());
  const [wordArray, setWordArray] = useState([]);

  const [{ data: dataFirst, error: errorFirst }] =
    useAxios('/api/vehicles_first');
  const [{ data: dataAll, error: errorAll }] = useAxios('/api/vehicles');
  const [vehicles, setVehicles] = useState([]);

  useEffect(() => {
    // Uses response to put data into array and set vehicle rows arr
    const bindData = (data) => {
      let arr = [];
      for (let i = 0; i < data.length; i++) {
        let instance = data[i];
        // Checks null data and saves data into array
        arr.push(
          {
            id: checkNull(instance['id']),
            name: checkNull(instance['name']),
            car_type: checkNull(instance['car_type']),
            manufacturer: checkNull(instance['manufacturer']),
            fuel_type: checkNull(instance['fuel_type']),
            annual_cost: checkNull(instance['annual_cost']),
            mpg: checkNull(instance['mpg']),
            model_year: checkNull(instance['model_year']),
            emissions: checkNull(instance['emissions']),
            range_mi: checkNull(instance['range_mi']),
            epa_fuel_economy_score: checkNull(
              instance['epa_fuel_economy_score']),
            engine_size: checkNull(instance['engine_size']),
            drive_type: checkNull(instance['drive_type']),
            avg_savings: checkNull(instance['avg_savings']),
          }
        )
      }
      setVehicles(arr);
    }

    if (dataAll !== undefined) {
      bindData(dataAll);
    } else if (dataFirst !== undefined) {
      bindData(dataFirst);
    }
  }, [dataAll, dataFirst]);

  if (errorFirst || errorAll) {
    return <Error />
  }

  // Make our columns for vehicle, listing attributes
  let columns = [
    {
      title: 'Name',
      field: 'name',
      filtering: false,
      render: rowData => <Link to={'/vehicles/' + rowData['id']} >
        {highlightWord(rowData.name)}</Link>,
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.name, value),
    },
    {
      title: 'Car Type',
      field: 'car_type',
      lookup: {},
      render: rowData => highlightWord(rowData.car_type),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.car_type, value),
    },
    {
      title: 'Manufacturer',
      field: 'manufacturer',
      lookup: {},
      render: rowData => highlightWord(rowData.manufacturer),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.manufacturer, value),
    },
    {
      title: 'Fuel Type',
      field: 'fuel_type',
      lookup: {},
      render: rowData => highlightWord(rowData.fuel_type),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.fuel_type, value),
    },
    {
      title: 'Annual Cost',
      field: 'annual_cost',
      filtering: false,
      render: rowData => highlightWord(rowData.annual_cost),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.annual_cost, value),
    },
    {
      title: 'MPG',
      field: 'mpg',
      filtering: false,
      render: rowData => highlightWord(rowData.mpg),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.mpg, value),
    },
    {
      title: 'Model Year',
      field: 'model_year',
      lookup: {},
      render: rowData => highlightWord(rowData.model_year),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.model_year, value),
    },
    {
      title: 'Emissions',
      field: 'emissions',
      filtering: false,
      render: rowData => highlightWord(rowData.emissions),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.emissions, value),
    },
    {
      title: 'Range Mi',
      field: 'range_mi',
      filtering: false,
      render: rowData => highlightWord(rowData.range_mi),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.range_mi, value),
    },
    {
      title: 'EPA Score',
      field: 'epa_fuel_economy_score',
      filtering: false,
      render: rowData => highlightWord(rowData.epa_fuel_economy_score),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.epa_fuel_economy_score, value),
    },
    {
      title: 'Engine Size',
      field: 'engine_size',
      filtering: false,
      render: rowData => highlightWord(rowData.engine_size),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.engine_size, value),
    },
    {
      title: 'Drive Type',
      field: 'drive_type',
      lookup: {},
      render: rowData => highlightWord(rowData.drive_type),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.drive_type, value),
    },
    {
      title: 'Average Savings',
      field: 'avg_savings',
      filtering: false,
      render: rowData => highlightWord(rowData.avg_savings),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.avg_savings, value),
    },
  ];

  // Checks if value passed in is null, if so returns N/A
  const checkNull = (value) => {
    if (value === null) {
      return "N/A";
    }
    return value.toString();
  }

  // Create maps to set our filtered data
  let manufacturers = new Map();
  let car_type = new Map();
  let fuel_type = new Map();
  let drive_type = new Map();
  let model_year = new Map();

  // Updates lookup dictionary in columns to our match maps
  const updateLookup = () => {
    let numMapping = {
      1: car_type, 2: manufacturers, 3: fuel_type, 6: model_year,
      11: drive_type
    };

    for (var numKey of Object.keys(numMapping)) {
      for (const [key, value] of numMapping[numKey].entries()) {
        columns[numKey]['lookup'][key] = value;
      }
    }
  }

  // Set our maps to update lookup for filtering
  for (let v = 0; v < vehicles.length; v++) {
    let instance = vehicles[v];
    if (instance['car_type'] !== null) {
      manufacturers.set(instance['manufacturer'], instance['manufacturer']);
      car_type.set(instance['car_type'], instance['car_type']);
      fuel_type.set(instance['fuel_type'], instance['fuel_type']);
      drive_type.set(instance['drive_type'], instance['drive_type']);
      model_year.set(instance['model_year'], instance['model_year']);
    }
    updateLookup();
  }

  // Icons for our material table
  const tableIcons = {
    Search: forwardRef((props, ref) =>
      <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) =>
      <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) =>
      <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) =>
      <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) =>
      <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) =>
      <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) =>
      <ArrowDownward {...props} ref={ref} />),
    Filter: forwardRef((props, ref) =>
      <FilterList {...props} ref={ref} />),
  };

  // Returns a highlighter tag of the phrase you want highlighted
  const highlightWord = (phrase) => {
    return (
      <Highlighter
        searchWords={tableRef.current.dataManager.searchText.split(" ")}
        textToHighlight={phrase}
      />
    )
  }

  // Used in our custom filter/search to handle multiple words
  const containsWord = (phrase, value) => {
    // Handles the filtering part of our custom 
    if (typeof (value) !== "string") {
      for (let i = 0; i < value.length; i++) {
        if (value[i] === phrase) {
          return true;
        } else {
          return false;
        }
      }
    }
    // Handles the searching part of our custom
    phrase = phrase.toLowerCase();
    for (let i = 0; i < wordArray.length; i++) {
      if (phrase.indexOf(wordArray[i]) !== -1) {
        return true;
      }
    }
    if (value.length === 0) { // returns full table if no filter/search
      return true;
    }
    return false;
  }

  return (
    <div>
      {/* display image above table */}
      <ModelImage myRef={myRef} isCar={true} />
      <div ref={myRef}>
        {/* display material table with vehicle data */}
        <MaterialTable title="Vehicles" data={vehicles} columns={columns}
          icons={tableIcons}
          components={{ FilterRow: props => <MTableFilterRow {...props} /> }}
          localization={{
            body: {
              emptyDataSourceMessage
                : <img src={loading} alt="loading" />
            }
          }}
          // customize table to sort/filter/search
          options={{
            filtering: true, sorting: true, exportButton: false,
            headerStyle: { backgroundColor: '#01579b', color: '#FFF' },
          }} tableRef={tableRef}
          onSearchChange={() => setWordArray(tableRef.current
            .dataManager.searchText.toLowerCase().split(" "))} />
        <br />
      </div>
    </div>
  )
}

export default Vehicles
