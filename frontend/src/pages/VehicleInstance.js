import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import tempImg from '../images/boltEV.jpg';
import { Link } from "react-router-dom";
import twitterLogo from '../images/twitterlogo.png';
import MaterialTable from "material-table";
import {
  Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward
} from "@material-ui/icons";
import LoadImg from '../images/loading.gif';
import Error from '../components/Error';

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';

// style for the Gridlist
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: '#a2ded0',
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into own layer on Chrome.
    transform: 'translateZ(0)',
  },
  title: {
    color: theme.palette.common.white,
    "&:hover": {
      color: "#5468ff"
    },
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, ' +
      'rgba(0,0,0,0) 100%)',
  },
}));

// First half of our fields in the vehicle instance
const fields1 = ["car_type", "manufacturer", "fuel_type", "annual_cost",
  "mpg", "model_year"];

// Second half of our fields in the vehicle instance
const fields2 = ["emissions", "range_mi", "epa_fuel_economy_score",
  "engine_size", "drive_type", "avg_savings"];

// Maps fields to a labeling
const fieldNames = {
  "car_type": "Car Type", "manufacturer": "Manufacturer",
  "fuel_type": "Fuel Type", "annual_cost": "Annual Fuel Cost (in USD)",
  "mpg": "Combined MPG (or equivalent)", "model_year": "Model Year",
  "emissions": "CO2 Emissions", "range_mi": "Range Mi",
  "epa_fuel_economy_score": "EPA Fuel Economy Score", "engine_size":
    "Engine Size", "drive_type": "Drive Type", "avg_savings":
    "Average Savings (in USD)"
};

// To display our vehicle instance
function VehicleInstance() {

  const classes = useStyles();

  let pages = window.location.pathname.split('/');
  let id = pages[2];

  const [{ data, loading, error }] = useAxios(`/api/vehicles/id=${id}`);
  const [tileData, setTileData] = useState([]);

  useEffect(() => {
    if (data !== undefined) {
      let array = data['energy_source'];
      // Puts the energy source instances into a grid list to display
      if (array !== undefined) {
        for (let i = 0; i < array.length; i++) {
          let info = [];
          axios.get(`/api/energy_sources/name=${array[i]}`)
            .then((r) => {
              info.push(
                {
                  img: r.data['image_url'],
                  title: array[i],
                  info: "",
                  cols: 2,
                  id: r.data["id"],
                },
              )
              setTileData((prevState) => prevState.concat(info));
            });
        }
      }
    }
  }, [data]);

  if (loading) return <div style={{ textAlign: 'center' }}>
    <img src={LoadImg} alt='loading' className='center' /> </div>
  if (error) {
    return <Error />
  }

  const stationRows = () => {
    // Creates the rows for our station table
    if (data['fuel_stations'] !== undefined) {
      let rows = [];
      for (let s = 0; s < data['fuel_stations'].length; s++) {
        rows.push({
          // add the station id, name, and map
          station_id: data['fuel_stations'][s],
          station_name: data['fuel_stations_names'][s],
          station_map: data['fuel_stations_addresses'][s]
        });

      }
      return rows;
    }
  }

  // Checks if image is null to use default image
  const imageUrl = (url) => {
    if (url === null) {
      return tempImg;
    }
    return url;
  }

  // Column for station table
  const stationColumns = [
    {
      title: 'Name',
      field: 'station_name',
      render: rowData => <Link to={'/stations/' + rowData['station_id']}>
        {rowData.station_name}</Link>,
    },
    {
      title: 'Map',
      field: 'station_map',
      render: rowData =>
        <iframe
          title="map"
          width="300"
          height="225"
          frameBorder="0" style={{ border: "0" }}
          src={"https://www.google.com/maps/embed/v1/place?key=" +
            "AIzaSyDdWiM3yF5ZagF745-dELROOc0Vs7LiK2k&q=" +
            rowData.station_map}
          allowFullScreen>
        </iframe>
    },
  ];

  // Icons for our material table
  const tableIcons = {
    Search: forwardRef((props, ref) =>
      <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) =>
      <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) =>
      <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) =>
      <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) =>
      <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) =>
      <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) =>
      <ArrowDownward {...props} ref={ref} />),
  };

  // Uses the vehicle instance fields to display/return our data
  const returnData = (fields) => {
    return fields.map(fields => (
      <p>
        <strong>{fieldNames[fields]}: </strong>
        {checkNull(data[fields])}
        {checkManufacturer(fields)}
      </p>
    ));
  }

  // Checks default vlaue of null to display N/A
  const checkNull = (value) => {
    if (value === null) {
      return "N/A";
    }
    return value + " ";
  }

  // Add twitter of manufacturer
  const checkManufacturer = (field) => {
    if (field === 'manufacturer') {
      return (
        <a className="twitter-timeline" href={"https://twitter.com/" +
          data['manufacturer'] + "?ref_src=twsrc%5Etfw"}>
          <img src={twitterLogo} width="30" height="30" alt="logo" />
        </a>
      )
    }
  }

  return (
    <div>
      <Container fluid={true} style={{ backgroundColor: '#a2ded0' }}>
        <br />
        <h2 className="text-center">{data['name']}</h2>
        <Row>
          {/* displays media data */}
          <Col >
            <div style={{ textAlign: 'center' }}>
              <img src={imageUrl(data['vehicle_image_url'])}
                alt="car" align='center' className="my_model mx-auto 
                d-block" style={{ marginTop: '20px', marginBottom: '60px' }}
                onError={(e) => {
                  e.target.onerror = null;
                  e.target.src = tempImg
                }} />
            </div>
          </Col>
        </Row>
        <Row style={{ backgroundColor: '#ffffff' }}>
          {/* displays vehicle instance data */}
          <Col>
            <br />
            {returnData(fields1)}
          </Col>
          <Col>
            <br />
            {returnData(fields2)}
          </Col>
        </Row>
        <br />
        <h4>Powered by: </h4>
        {/* displays our energy sources as a gridlist */}
        <div className={classes.root}>
          <GridList className={classes.gridList} cols={2.5}>
            {tileData.map((tile) => (
              <GridListTile key={tile.img} style={{ width: 300 }}>
                <Link to={"/energy/" + tile.id}>
                  <img src={tile.img} alt={tile.title}
                    style={{ width: '100%', height: '100%' }} />
                  <GridListTileBar
                    title={tile.title}
                    classes={{ root: classes.titleBar, title: classes.title, }}
                  />
                </Link>
              </GridListTile>
            ))}
          </GridList>
        </div>

        <br />
        {/* displays station table */}
        <MaterialTable
          title="Supported Fuel Stations"
          data={stationRows()}
          columns={stationColumns}
          icons={tableIcons}
          options={{ exportButton: false, headerStyle: 
            { backgroundColor: '#01579b', color: '#FFF' },
          }}
        />
        <br />
      </Container>
    </div>
  )
}

export default VehicleInstance;
