// Default Imports
import React from "react";

// Import each visualization component
import Vis1 from "../visComponents/econyoom_vis_1";
import Vis2 from "../visComponents/econyoom_vis_2";
import Vis3 from "../visComponents/econyoom_vis_3";
import Vis4 from "../visComponents/econyoom_vis_4";
import Vis5 from "../visComponents/econyoom_vis_5";
import Vis6 from "../visComponents/econyoom_vis_6";

export default Visualizations;

function Visualizations() {
  // Constant variables and data loading for the visualization display
  const vis1_outer_radius = 350;

  return (
    <main className="vis-container" style={{ textAlign: "center" }}>
      <br />
      <h1>Will fix by presentation</h1>
      <h2>EcoNyoom Visualizations</h2>
      <strong>Percentage of Cars Used by Each
                Energy Source</strong>
      <Vis1 o={vis1_outer_radius} i={0} />
      <strong>Average Savings Over the Average Gasoline
                Car for Each Fuel Type</strong>
      <Vis2 />
      <strong>Average Mileage (or equivalent in energy)
                for Each Fuel Type</strong>
      <Vis3 />
      <hr />
      <h2>CrimeStats Visualizations</h2>
      <strong>Police Officers to Population</strong>
      <Vis4 />
      <strong>Number of Police Officers
                to Number of Crimes</strong>
      <Vis5 />
      <strong>Percentage of Crimes Committed
                by Each Race</strong>
      <Vis6 o={vis1_outer_radius} i={0} />

    </main>
  )
}
