import React, { forwardRef, useEffect, useState } from 'react';
import useAxios from 'axios-hooks';
import MaterialTable from "material-table";
import {
  Search, FirstPage, LastPage, ChevronLeft, ChevronRight,
  Clear, ArrowDownward, FilterList
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import loading from '../images/loading.gif'
import Highlighter from "react-highlight-words";
import MTableFilterRow from '../components/filterOveride'
import ModelImage from '../components/ModelImage';
import Error from '../components/Error';

// Display our station model page
function Stations() {

  const [myRef] = useState(React.createRef());
  const [tableRef] = useState(React.createRef());
  const [wordArray, setWordArray] = useState([]);

  const [{ data: dataFirst, error: errorFirst }] =
    useAxios('/api/fuel_stations_first');
  const [{ data: dataAll, error: errorAll }] = useAxios('/api/fuel_stations');
  const [stations, setStations] = useState([]);

  useEffect(() => {
    // Uses response to put data into array and set vehicle rows arr
    const bindData = (data) => {
      let arr = [];
      for (let i = 0; i < data.length; i++) {
        let instance = data[i];
        // Checks null data and saves data into array
        arr.push(
          {
            id: checkNull(instance["id"]),
            station_name: checkNull(instance['station_name']),
            access: checkNull(instance["access"]),
            fuel_type: checkNull(instance["fuel_type"]),
            facility_type: checkNull(instance["facility_type"]),
            address: checkNull(instance["address"]),
            cards_accepted: checkNull(instance["cards_accepted"]),
            ev_network: checkNull(instance["ev_network"]),
            state: checkNull(instance["state"]),
            zip_code: checkNull(instance["zip_code"]),
            country: checkNull(instance["country"]),
            phone_number: checkNull(instance["phone_number"]),
            status: checkNull(instance["status"]),
          }
        )
      }
      setStations(arr);
    }

    if (dataAll !== undefined) {
      bindData(dataAll);
    } else if (dataFirst !== undefined) {
      bindData(dataFirst);
    }
  }, [dataAll, dataFirst]);

  if (errorFirst || errorAll) {
    return <Error />
  }

  // Make our columns for station, listing attributes
  const columns = [
    {
      title: 'Name',
      field: 'station_name',
      filtering: false,
      render: rowData => <Link to={'/stations/' + rowData['id']} >
        {highlightWord(rowData.station_name)}</Link>,
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.station_name, value),
    },
    {
      title: 'Access',
      field: 'access',
      lookup: {},
      render: rowData => highlightWord(rowData.access),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.access, value),
    },
    {
      title: 'Fuel Type',
      field: 'fuel_type',
      lookup: {},
      render: rowData => highlightWord(rowData.fuel_type),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.fuel_type, value),
    },
    {
      title: 'Facility Type',
      field: 'facility_type',
      lookup: {},
      render: rowData => highlightWord(rowData.facility_type),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.facility_type, value),
    },
    {
      title: 'Address',
      field: 'address',
      filtering: false,
      render: rowData => highlightWord(rowData.address),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.address, value),
    },
    {
      title: 'Cards Accepted',
      field: 'cards_accepted',
      lookup: {},
      render: rowData => highlightWord(rowData.cards_accepted),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.cards_accepted, value),
    },
    {
      title: 'EV Network',
      field: 'ev_network',
      lookup: {},
      render: rowData => highlightWord(rowData.ev_network),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.ev_network, value),
    },
    {
      title: 'State',
      field: 'state',
      lookup: {},
      render: rowData => highlightWord(rowData.state),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.state, value),
    },
    {
      title: 'Zip Code',
      field: 'zip_code',
      filtering: false,
      render: rowData => highlightWord(rowData.zip_code),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.zip_code, value),
    },
    {
      title: 'Country',
      field: 'country',
      filtering: false,
      render: rowData => highlightWord(rowData.country),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.country, value),
    },
    {
      title: 'Phone Number',
      field: 'phone_number',
      filtering: false,
      render: rowData => highlightWord(rowData.phone_number),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.phone_number, value),

    },
    {
      title: 'Status',
      field: 'status',
      lookup: {},
      render: rowData => highlightWord(rowData.status),
      customFilterAndSearch: (value, rowData) =>
        containsWord(rowData.status, value),
    },
  ];

  // Check null to display as N/A
  const checkNull = (value) => {
    if (value === null) {
      return "N/A";
    }
    return value.toString();
  }

  // Create maps to set our filtered data
  let access = new Map();
  let fuel_type = new Map();
  let facility_type = new Map();
  let cards_accepted = new Map();
  let ev_network = new Map();
  let state = new Map();
  let status = new Map();

  // Updates lookup dictionary in columns to our match maps
  const updateLookup = () => {
    let numMapping = {
      1: access, 2: fuel_type, 3: facility_type, 5: cards_accepted,
      6: ev_network, 7: state, 11: status
    };

    for (var numKey of Object.keys(numMapping)) {
      for (const [key, value] of numMapping[numKey].entries()) {
        columns[numKey]['lookup'][key] = value;
      }
    }
  }

  // Set our maps to update lookup for filtering
  for (let v = 0; v < stations.length; v++) {
    let instance = stations[v];

    access.set(instance['access'], instance['access']);
    fuel_type.set(instance['fuel_type'], instance['fuel_type']);
    facility_type.set(instance['facility_type'], instance['facility_type']);
    cards_accepted.set(instance['cards_accepted'], instance['cards_accepted']);
    ev_network.set(instance['ev_network'], instance['ev_network']);
    state.set(instance['state'], instance['state']);
    status.set(instance['status'], instance['status']);
    updateLookup();
  }

  // Icons for our material table
  const tableIcons = {
    Search: forwardRef((props, ref) =>
      <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) =>
      <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) =>
      <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) =>
      <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) =>
      <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) =>
      <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) =>
      <ArrowDownward {...props} ref={ref} />),
    Filter: forwardRef((props, ref) =>
      <FilterList {...props} ref={ref} />),
  };

  // Returns a highlighter tag of the phrase you want highlighted
  const highlightWord = (phrase) => {
    return (
      <Highlighter
        searchWords={tableRef.current.dataManager.searchText.split(" ")}
        textToHighlight={phrase}
      />
    )
  }

  // Used for our custom search to handle multiple terms
  const containsWord = (phrase, value) => {
    // Handles the filtering part of our custom 
    if (typeof (value) !== "string") {
      for (let i = 0; i < value.length; i++) {
        if (value[i] === phrase) {
          return true;
        } else {
          return false;
        }
      }
    }
    // Handles the searching part of our custom
    phrase = phrase.toLowerCase();
    for (let i = 0; i < wordArray.length; i++) {
      if (phrase.indexOf(wordArray[i]) !== -1) {
        return true;
      }
    }
    if (value.length === 0) { // returns full table if no filter/search
      return true;
    }
    return false;
  }

  return (
    <div>
      {/* display image above table */}
      <ModelImage myRef={myRef} isCar={false} />
      <div ref={myRef}>
        {/* displays material table of stations */}
        <MaterialTable title="Fueling Stations" data={stations}
          columns={columns} icons={tableIcons}
          components={{ FilterRow: props => <MTableFilterRow {...props} /> }}
          localization={{
            body: {
              emptyDataSourceMessage:
                <img src={loading} alt="loading" />
            }
          }}
          // customize our table to fit filter/sort/search
          options={{
            filtering: true, sorting: true, exportButton: false,
            headerStyle: { backgroundColor: '#01579b', color: '#FFF' },
          }}
          tableRef={tableRef}
          onSearchChange={() => setWordArray(
          tableRef.current.dataManager.searchText.toLowerCase().split(" "))} />
        <br />
      </div>
    </div>
  )
}

export default Stations;
