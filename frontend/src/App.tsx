import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './css/bootstrap.min.css';

import Header from './components/Header';
import NavBar from './components/NavBar';
import EcoCarousel from './components/homeComponents/EcoCarousel';
import Home from './components/homeComponents/Home';
import Footer from './components/Footer.js';

// Import our pages
import About from './pages/About';
import Vehicles from './pages/Vehicles.js';
import Stations from './pages/Stations.js';
import Energy from './pages/Energy.js';
import VehicleInstance from './pages/VehicleInstance.js';
import StationInstance from './pages/StationInstance.js';
import EnergyInstance from './pages/EnergyInstance.js';
import SearchPage from './pages/SearchPage.js';
import Visualizations from './pages/Visualizations.js';

function App() {
    return (
      <Router>
      <div>
          <Header />
          <NavBar />
          <Route exact path="/" render={props => (
          <React.Fragment>
            <EcoCarousel />
            <Home />
          </React.Fragment>
          )}/>
          
          {/* Route pages in our site */}
          <Route exact path="/about" component={About} />
          <Route exact path="/vehicles" component={Vehicles} />
          <Route exact path="/stations" component={Stations} />
          <Route exact path="/energy" component={Energy} />
          <Route exact path="/vehicles/:id" component={VehicleInstance} />
          <Route exact path="/stations/:id" component={StationInstance} />
          <Route exact path="/energy/:id" component={EnergyInstance} />
          <Route exact path="/search" component={SearchPage} />
          <Route exact path="/visualizations" component={Visualizations} />
          <Footer />
      </div>
      </Router>
    );
}

export default App;
