.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash


##### DOCKER #####

# Runs the backend docker image
docker-run:
	docker run \
		--name econyoom_container \
		--rm \
		-d \
		-p 80:80 \
		econyoom

# Stops the backend docker container
docker-stop:
	docker stop econyoom_container

# Builds the backend docker image 4
docker-build:
	docker build --no-cache -t econyoom:latest .

# Builds/rebuilds the docker-compose container
docker-compose-build:
	docker-compose build

# Create and start container within docker-compose container
docker-compose-up:
	docker-compose up

# Removes the docker-compose container
docker-compose-down:
	docker-compose down



##### LOCAL #####

# runs the frontend app locally
run-frontend:
	cd frontend && yarn start

# sets up installations and dependencies required for frontend
frontend-build:
	cd frontend && yarn install 
	cd frontend && yarn build

# tests for frontend
frontend-tests:
	cd frontend && yarn test

# runs the backend app locally
run-backend:
	cd backend && python3 api.py

# sets up installations and dependencies required for backend
backend-build:
	cd frontend && yarn install && yarn build
	cd backend && pip3 install -r requirements.txt

backend-tests: 
	python3 -m backend.tests.models_test